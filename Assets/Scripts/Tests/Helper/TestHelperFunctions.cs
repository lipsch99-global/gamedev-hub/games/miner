﻿using System.Reflection;
using NUnit.Framework;

namespace Tests.Helper
{
    public class TestHelperFunctions
    {
        public static void SetPrivateValue(ref object objectToChange, string fieldName, object value)
        {
            var fieldInfo = objectToChange.GetType().GetField(fieldName, BindingFlags.NonPublic |
                                                                         BindingFlags.Instance);
            if (fieldInfo != null)
            {
                fieldInfo.SetValue(objectToChange, value);
            }
            else
            {
                Assert.Fail($"No field {fieldName} found.");
            }
        }

        public static void SetPrivateValueOfBaseType(ref object objectToChange, string fieldName, object value)
        {
            var fieldInfo = objectToChange.GetType().BaseType?.GetField(fieldName, BindingFlags.NonPublic |
                                                                                   BindingFlags.Instance);
            if (fieldInfo != null)
            {
                fieldInfo.SetValue(objectToChange, value);
            }
            else
            {
                Assert.Fail($"No field {fieldName} found.");
            }
        }
    }
}