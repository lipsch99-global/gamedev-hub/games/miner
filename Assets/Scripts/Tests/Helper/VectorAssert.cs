﻿using System;
using NUnit.Framework;
using UnityEngine;

namespace Tests.Helper
{
    public static class VectorAssert
    {
        public static void AreEqual(Vector2 expected, Vector2 actual, float delta = 0f)
        {
            if (Math.Abs(expected.x - actual.x) > delta || Math.Abs(expected.y - actual.y) > delta)
            {
                Assert.Fail($"{expected.ToString("F4")} and {actual.ToString("F4")} are not equal.");
            }
        }
        
        public static void AreEqual(Vector3 expected, Vector3 actual, float delta = 0f)
        {
            if (Math.Abs(expected.x - actual.x) > delta || Math.Abs(expected.y - actual.y) > delta || Math.Abs(expected.z - actual.z) > delta)
            {
                Assert.Fail($"{expected.ToString("F4")} and {actual.ToString("F4")} are not equal.");
            }
        }
        
    }
}