﻿using Helper;
using NUnit.Framework;
using UnityEngine;

namespace Tests.Helper
{
    public class HelperFunctionTests
    {
        public class DirectionFromRotation
        {
            [Test]
            public void _negative_270_right()
            {
                var result = HelperFunctions.GetDirectionFromRotation(-270);
                VectorAssert.AreEqual(Vector2.right, result, 0.0001f);
            }
            [Test]
            public void _negative_180_down()
            {
                var result = HelperFunctions.GetDirectionFromRotation(-180);
                VectorAssert.AreEqual(Vector2.down, result, 0.0001f);
            }
            [Test]
            public void _negative_90_left()
            {
                var result = HelperFunctions.GetDirectionFromRotation(-90);
                VectorAssert.AreEqual(Vector2.left, result, 0.0001f);
            }
            [Test]
            public void _0_up()
            {
                var result = HelperFunctions.GetDirectionFromRotation(0);
                VectorAssert.AreEqual(Vector2.up, result, 0.0001f);
            }

            [Test]
            public void _90_right()
            {
                var result = HelperFunctions.GetDirectionFromRotation(90);
                VectorAssert.AreEqual(Vector2.right, result, 0.0001f);
            }

            [Test]
            public void _180_down()
            {
                var result = HelperFunctions.GetDirectionFromRotation(180);
                VectorAssert.AreEqual(Vector2.down, result, 0.0001f);
            }

            [Test]
            public void _270_left()
            {
                var result = HelperFunctions.GetDirectionFromRotation(270);
                VectorAssert.AreEqual(Vector2.left, result, 0.0001f);
            }

            [Test]
            public void _360_up()
            {
                var result = HelperFunctions.GetDirectionFromRotation(360);
                VectorAssert.AreEqual(Vector2.up, result, 0.0001f);
            }
        }

        public class RotationFromDirection
        {
            [Test]
            public void _up_0()
            {
                var result = HelperFunctions.GetRotationFromDirection(Vector2.up);
                Assert.AreEqual(0, result, Mathf.Epsilon);
            }

            [Test]
            public void _right_90()
            {
                var result = HelperFunctions.GetRotationFromDirection(Vector2.right);
                Assert.AreEqual(90, result, Mathf.Epsilon);
            }

            [Test]
            public void _down_180()
            {
                var result = HelperFunctions.GetRotationFromDirection(Vector2.down);
                Assert.AreEqual(180, result, Mathf.Epsilon);
            }

            [Test]
            public void _left_270()
            {
                var result = HelperFunctions.GetRotationFromDirection(Vector2.left);
                Assert.AreEqual(270, result, Mathf.Epsilon);
            }
        }
    }
}