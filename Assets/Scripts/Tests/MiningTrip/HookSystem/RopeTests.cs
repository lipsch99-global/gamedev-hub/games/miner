﻿using System.Reflection;
using MiningTrip.HookSystem;
using NUnit.Framework;
using Tests.Helper;
using UnityEngine;

namespace Tests.MiningTrip.HookSystem
{
    public class RopeTests
    {
        public class Align
        {
            private GameObject _ropeGo;
            private Rope _rope;

            [SetUp]
            public void BeforeEach()
            {
                _ropeGo = new GameObject();
                _rope = _ropeGo.AddComponent<Rope>();

                //set the private field ropeRenderer using reflection
                var type = typeof(Rope);
                var fieldInfo = type.GetField("ropeRenderer", BindingFlags.NonPublic |
                                                              BindingFlags.Instance);
                if (fieldInfo != null)
                {
                    fieldInfo.SetValue(_rope, _ropeGo.AddComponent<SpriteRenderer>());
                }
                else
                {
                    Assert.Fail("No field ropeRenderer found.");
                }
            }

            [Test]
            public void _x1_1_y1_1_x2_negative_1_y2_negative_1_has_position_x_0_y_0_z_0()
            {
                var a = new Vector2(1, 1);
                var b = new Vector2(-1, -1);
                _rope.Align(a, b);
                var expected = new Vector3(0, 0, 0);
                VectorAssert.AreEqual(_ropeGo.transform.position, expected, 0.0001f);
            }

            [Test]
            public void _x1_1_y1_1_x2_negative_1_y2_negative_1_has_rotation_315()
            {
                var a = new Vector2(1, 1);
                var b = new Vector2(-1, -1);
                _rope.Align(a, b);
                Assert.AreEqual(315, _ropeGo.transform.eulerAngles.z);
            }
        }
    }
}