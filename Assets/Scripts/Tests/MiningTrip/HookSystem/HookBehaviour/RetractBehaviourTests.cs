﻿using MiningTrip.HookSystem;
using MiningTrip.HookSystem.Behaviour;
using MiningTrip.HookSystem.Hook;
using MiningTrip;
using NUnit.Framework;
using UnityEngine;

namespace Tests.MiningTrip.HookSystem.HookBehaviour
{
    public class RetractBehaviourTests
    {
        public class GetPosition
        {
            private Hook _hook;
            private RetractHook _retractBehaviour;
            private State _state;
            private Vector2 _newPosition;
            private GameObject _characterGo;

            [SetUp]
            public void BeforeEach()
            {
                _characterGo = new GameObject();
                _characterGo.transform.position = Vector3.zero;
                _characterGo.AddComponent<Character>();
                
                var hookGo = new GameObject();
                _hook = hookGo.AddComponent<Hook>();
                _retractBehaviour =
                    GetRetractBehaviourWithSetValues(global::MiningTrip.HookSystem.Behaviour.HookBehaviour.UpdatePattern.Position,
                        3, 2);
                _state = _retractBehaviour.GetNewStateInstance();
            }


            [Test]
            public void _New_Position_Is_In_Same_Direction_From_Character_Position_x_0_y_negative_5()
            {
                var startPos = new Vector2(0, -5);
                _hook.SetPosition(startPos);
                var newPos = _retractBehaviour.GetNewPosition(ref _state, _hook);

                Assert.AreEqual(1, Vector2.Dot(startPos.normalized, newPos.normalized), Mathf.Epsilon);
            }

            [Test]
            public void _New_Position_Is_In_Same_Direction_From_Character_Position_x_0_y_5()
            {
                var startPos = new Vector2(0, 5);
                _hook.SetPosition(startPos);
                var newPos = _retractBehaviour.GetNewPosition(ref _state, _hook);

                Assert.AreEqual(1, Vector2.Dot(startPos.normalized, newPos.normalized), Mathf.Epsilon);
            }

            [Test]
            public void _New_Position_Is_In_Same_Direction_From_Character_Position_x_5_y_0()
            {
                var startPos = new Vector2(5, 0);
                _hook.SetPosition(startPos);
                var newPos = _retractBehaviour.GetNewPosition(ref _state, _hook);

                Assert.AreEqual(1, Vector2.Dot(startPos.normalized, newPos.normalized), Mathf.Epsilon);
            }

            [Test]
            public void _New_Position_Is_In_Same_Direction_From_Character_Position_x_negative_5_y_0()
            {
                var startPos = new Vector2(-5, 0);
                _hook.SetPosition(startPos);
                var newPos = _retractBehaviour.GetNewPosition(ref _state, _hook);

                Assert.AreEqual(1, Vector2.Dot(startPos.normalized, newPos.normalized), Mathf.Epsilon);
            }

            [Test]
            public void _Position_Closer_Than_Limit_Should_Return_Finished_x_0_y_1()
            {
                var startPos = new Vector2(0, 1);
                _hook.SetPosition(startPos);
                var behaviourFinished = _retractBehaviour.IsBehaviourFinished(_state, _hook);

                Assert.AreEqual(true, behaviourFinished);
            }
            
            [Test]
            public void _Position_Farther_Than_Limit_Should_Return_Finished_x_0_y_3()
            {
                var startPos = new Vector2(0, 3);
                _hook.SetPosition(startPos);
                var behaviourFinished = _retractBehaviour.IsBehaviourFinished(_state, _hook);

                Assert.AreEqual(false, behaviourFinished);
            }
            
            [Test]
            public void _Position_Closer_Than_Limit_Should_Return_Finished_x_0_y_negative_1()
            {
                var startPos = new Vector2(0, -1);
                _hook.SetPosition(startPos);
                var behaviourFinished = _retractBehaviour.IsBehaviourFinished(_state, _hook);

                Assert.AreEqual(true, behaviourFinished);
            }
            
            [Test]
            public void _Position_Farther_Than_Limit_Should_Return_Finished_x_0_y_negative_3()
            {
                var startPos = new Vector2(0, -3);
                _hook.SetPosition(startPos);
                var behaviourFinished = _retractBehaviour.IsBehaviourFinished(_state, _hook);

                Assert.AreEqual(false, behaviourFinished);
            }
            
            [Test]
            public void _Position_Closer_Than_Limit_Should_Return_Finished_x_1_y_0()
            {
                var startPos = new Vector2(1, 0);
                _hook.SetPosition(startPos);
                var behaviourFinished = _retractBehaviour.IsBehaviourFinished(_state, _hook);

                Assert.AreEqual(true, behaviourFinished);
            }
            
            [Test]
            public void _Position_Farther_Than_Limit_Should_Return_Finished_x_3_y0()
            {
                var startPos = new Vector2(3, 0);
                _hook.SetPosition(startPos);
                var behaviourFinished = _retractBehaviour.IsBehaviourFinished(_state, _hook);

                Assert.AreEqual(false, behaviourFinished);
            }
            
            [Test]
            public void _Position_Closer_Than_Limit_Should_Return_Finished_x__negative_1_y_0()
            {
                var startPos = new Vector2(-1, 0);
                _hook.SetPosition(startPos);
                var behaviourFinished = _retractBehaviour.IsBehaviourFinished(_state, _hook);

                Assert.AreEqual(true, behaviourFinished);
            }
            
            [Test]
            public void _Position_Farther_Than_Limit_Should_Return_Finished_x_negative_3_y_0()
            {
                var startPos = new Vector2(-3, 0);
                _hook.SetPosition(startPos);
                var behaviourFinished = _retractBehaviour.IsBehaviourFinished(_state, _hook);

                Assert.AreEqual(false, behaviourFinished);
            }

            private static RetractHook GetRetractBehaviourWithSetValues(
                global::MiningTrip.HookSystem.Behaviour.HookBehaviour.UpdatePattern updateType, float speed, float targetDistance)
            {
                var retractBehaviour = (object) ScriptableObject.CreateInstance<RetractHook>();
                Helper.TestHelperFunctions.SetPrivateValue(ref retractBehaviour, "retractSpeed", speed);
                Helper.TestHelperFunctions.SetPrivateValue(ref retractBehaviour, "targetDistanceFromCharacter",
                    targetDistance);
                Helper.TestHelperFunctions.SetPrivateValueOfBaseType(ref retractBehaviour, "updateType", updateType);

                return (RetractHook) retractBehaviour;
            }
        }
    }
}