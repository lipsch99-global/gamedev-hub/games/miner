﻿using System.Collections;
using MiningTrip;
using MiningTrip.HookSystem;
using MiningTrip.HookSystem.Behaviour;
using MiningTrip.HookSystem.Hook;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests.MiningTrip.HookSystem.HookBehaviour
{
    public class HookRotatorTests
    {
        public class GetRotation
        {
            private Hook _hook;
            private HookRotator _rotatorBehaviour;
            private State _state;
            private float _newRotation;
            private GameObject _characterGo;

            [SetUp]
            public void BeforeEach()
            {
                _characterGo = new GameObject();
                _characterGo.transform.position = Vector3.zero;
                _characterGo.AddComponent<Character>();
                
                var hookGo = new GameObject();
                _hook = hookGo.AddComponent<Hook>();
                _rotatorBehaviour =
                    GetRotatorBehaviourWithSetValues(global::MiningTrip.HookSystem.Behaviour.HookBehaviour.UpdatePattern
                        .Rotation);
                _state = _rotatorBehaviour.GetNewStateInstance();

            }


            [Test]
            public void _Hook_Position_x_0_y_negative_1_Hook_Rotation_is_180()
            {
                _hook.SetPosition(new Vector2(0, -1));
                Assert.AreEqual(180, _rotatorBehaviour.GetNewRotation(ref _state, _hook));
            }

            [Test]
            public void _Hook_Position_x_0_y_1_Hook_Rotation_is_0()
            {
                _hook.SetPosition(new Vector2(0, 1));
                Assert.AreEqual(0, _rotatorBehaviour.GetNewRotation(ref _state, _hook));
            }

            [Test]
            public void _Hook_Position_x_1_y_0_Hook_Rotation_is_90()
            {
                _hook.SetPosition(new Vector2(1, 0));
                Assert.AreEqual(90, _rotatorBehaviour.GetNewRotation(ref _state, _hook));
            }

            [Test]
            public void _Hook_Position_x_negative_1_y_0_Hook_Rotation_is_270()
            {
                _hook.SetPosition(new Vector2(-1, 0));
                Assert.AreEqual(270, _rotatorBehaviour.GetNewRotation(ref _state, _hook));
            }

            private static HookRotator GetRotatorBehaviourWithSetValues(
                global::MiningTrip.HookSystem.Behaviour.HookBehaviour.UpdatePattern updateType)
            {
                var rotatorBehaviour = (object) ScriptableObject.CreateInstance<HookRotator>();
                Helper.TestHelperFunctions.SetPrivateValueOfBaseType(ref rotatorBehaviour, "updateType", updateType);

                return (HookRotator) rotatorBehaviour;
            }
        }
    }
}