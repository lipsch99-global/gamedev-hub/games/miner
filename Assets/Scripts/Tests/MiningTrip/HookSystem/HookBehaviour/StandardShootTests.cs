﻿using System;
using MiningTrip;
using MiningTrip.HookSystem;
using MiningTrip.HookSystem.Behaviour;
using MiningTrip.HookSystem.Hook;
using NUnit.Framework;
using Tests.Helper;
using UnityEngine;

namespace Tests.MiningTrip.HookSystem.HookBehaviour
{
    public class StandardShootTests
    {
        public class GetPosition
        {
            private Hook _hook;
            private StandardShoot _shootBehaviour;
            private State _state;
            private Vector2 _newPosition;

            [SetUp]
            public void BeforeEach()
            {
                var hookGo = new GameObject();
                _hook = hookGo.AddComponent<Hook>();
                _shootBehaviour =
                    GetShootBehaviourWithSetValues(global::MiningTrip.HookSystem.Behaviour.HookBehaviour.UpdatePattern.Position,
                        3);
                _state = _shootBehaviour.GetNewStateInstance();
            }


            [Test]
            public void _LastExecution_0_Rotation_180_is_in_dir_x_0_y_negative_1()
            {
                _hook.SetRotation(180);
                _state.LastExecution = 0;
                _newPosition = _shootBehaviour.GetNewPosition(ref _state, _hook);
                var dotProduct = Vector2.Dot(_newPosition.normalized, new Vector2(0, -1));
                Assert.AreEqual(1, dotProduct, Mathf.Epsilon);
            }
            
            [Test]
            public void _LastExecution_0_Rotation_90_is_in_dir_x_1_y_0()
            {
                _hook.SetRotation(90);
                _state.LastExecution = 0;
                _newPosition = _shootBehaviour.GetNewPosition(ref _state, _hook);
                var dotProduct = Vector2.Dot(_newPosition.normalized, new Vector2(1, 0));
                Assert.AreEqual(1, dotProduct, Mathf.Epsilon);
            }
            
            [Test]
            public void _LastExecution_0_Rotation_0_is_in_dir_x_0_y_1()
            {
                _hook.SetRotation(0);
                _state.LastExecution = 0;
                _newPosition = _shootBehaviour.GetNewPosition(ref _state, _hook);
                var dotProduct = Vector2.Dot(_newPosition.normalized, new Vector2(0, 1));
                Assert.AreEqual(1, dotProduct, Mathf.Epsilon);
            }
            
            [Test]
            public void _LastExecution_0_Rotation_270_is_in_dir_x_negative_1_y_0()
            {
                _hook.SetRotation(270);
                _state.LastExecution = 0;
                _newPosition = _shootBehaviour.GetNewPosition(ref _state, _hook);
                var dotProduct = Vector2.Dot(_newPosition.normalized, new Vector2(-1, 0));
                Assert.AreEqual(1, dotProduct, Mathf.Epsilon);
            }

            private static StandardShoot GetShootBehaviourWithSetValues(
                global::MiningTrip.HookSystem.Behaviour.HookBehaviour.UpdatePattern updateType, float speed)
            {
                var shootBehaviour = (object) ScriptableObject.CreateInstance<StandardShoot>();
                Helper.TestHelperFunctions.SetPrivateValue(ref shootBehaviour, "shootSpeed", speed);
                Helper.TestHelperFunctions.SetPrivateValueOfBaseType(ref shootBehaviour, "updateType", updateType);

                return (StandardShoot) shootBehaviour;
            }
        }
    }
}