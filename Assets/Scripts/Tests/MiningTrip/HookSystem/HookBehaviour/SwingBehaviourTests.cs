﻿using System;
using System.Reflection;
using MiningTrip;
using MiningTrip.HookSystem;
using MiningTrip.HookSystem.Behaviour;
using MiningTrip.HookSystem.Hook;
using NUnit.Framework;
using Tests.Helper;
using UnityEngine;

namespace Tests.MiningTrip.HookSystem.HookBehaviour
{
    public class SwingBehaviourTests
    {
        public class GetPosition
        {
            private Hook _hook;
            private StandardSwing _swingBehaviour;
            private State _state;
            private GameObject _characterGo;
            private Vector2 _newPosition;

            [SetUp]
            public void BeforeEach()
            {
                var hookGo = new GameObject();
                _hook = hookGo.AddComponent<Hook>();
                _swingBehaviour =
                    GetSwingBehaviourWithSetValues(global::MiningTrip.HookSystem.Behaviour.HookBehaviour.UpdatePattern.Position,
                        1, 1, 90);
                _characterGo = new GameObject();
                _characterGo.transform.position = Vector3.zero;
                _characterGo.AddComponent<Character>();
                _state = _swingBehaviour.GetNewStateInstance();
            }


            [Test]
            public void _LastExecution_0_returns_x_0_7071_y_negative_0_7071()
            {
                CharacterState characterState = (CharacterState) _state;
                characterState.LastExecution = 0;
                _newPosition = _swingBehaviour.GetNewPosition(ref _state, _hook);

                VectorAssert.AreEqual(new Vector2(0.7071f, -0.7071f), _newPosition, 0.0001f);
            }

            [Test]
            public void _LastExecution_5_returns_x_0_2209_y_negative_0_9753()
            {
                CharacterState characterState = (CharacterState) _state;
                characterState.LastExecution = 5;
                _newPosition = _swingBehaviour.GetNewPosition(ref _state, _hook);

                VectorAssert.AreEqual(new Vector2(0.2209f, -0.9753f), _newPosition, 0.0001f);
            }
            
            [Test]
            public void _LastExecution_10_returns_x_negative_0_6123_y_negative_0_7906()
            {
                CharacterState characterState = (CharacterState) _state;
                characterState.LastExecution = 10;
                _newPosition = _swingBehaviour.GetNewPosition(ref _state, _hook);

                VectorAssert.AreEqual(new Vector2(-0.6123f, -0.7906f), _newPosition, 0.0001f);
            }
            
            [Test]
            public void _LastExecution_1000_returns_x_0_4275_y_negative_0_9040()
            {
                CharacterState characterState = (CharacterState) _state;
                characterState.LastExecution = 1000;
                _newPosition = _swingBehaviour.GetNewPosition(ref _state, _hook);

                VectorAssert.AreEqual(new Vector2(0.4275f, -0.9040f), _newPosition, 0.0001f);
            }

            private static StandardSwing GetSwingBehaviourWithSetValues(
                global::MiningTrip.HookSystem.Behaviour.HookBehaviour.UpdatePattern updateType, float speed, float distance,
                float range)
            {
                var swingBehaviour = (object) ScriptableObject.CreateInstance<StandardSwing>();
                Helper.TestHelperFunctions.SetPrivateValue(ref swingBehaviour, "swingSpeed", speed);
                Helper.TestHelperFunctions.SetPrivateValue(ref swingBehaviour, "swingDistance", distance);
                Helper.TestHelperFunctions.SetPrivateValue(ref swingBehaviour, "swingRange", range);
                Helper.TestHelperFunctions.SetPrivateValueOfBaseType(ref swingBehaviour, "updateType", updateType);

                return (StandardSwing) swingBehaviour;
            }
            
        }
    }
}