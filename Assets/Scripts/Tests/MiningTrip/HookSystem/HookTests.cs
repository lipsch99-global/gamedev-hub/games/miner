﻿using MiningTrip.HookSystem;
using MiningTrip.HookSystem.Hook;
using NUnit.Framework;
using Tests.Helper;
using UnityEngine;

namespace Tests.MiningTrip.HookSystem
{
    public class HookTests
    {
        public class GetPosition
        {
            private GameObject _hookGo;
            private Hook _hook;

            [SetUp]
            public void BeforeEach()
            {
                _hookGo = new GameObject();
                _hook = _hookGo.AddComponent<Hook>();
            }

            [Test]
            public void _x_50_y_50_z_50_returns_x_50_y_50()
            {
                _hookGo.transform.position = new Vector3(50, 50, 50);
                var position = _hook.GetPosition();
                VectorAssert.AreEqual(new Vector2(50, 50), position, 0.0001f);
            }

            [Test]
            public void _x_negative_50_y_50_z_50_returns_x_negative_50_y_50()
            {
                _hookGo.transform.position = new Vector3(-50, 50, 50);
                var position = _hook.GetPosition();
                VectorAssert.AreEqual(new Vector2(-50, 50), position, 0.0001f);
            }
        }

        public class SetPosition
        {
            private GameObject _hookGo;
            private Hook _hook;

            [SetUp]
            public void BeforeEach()
            {
                _hookGo = new GameObject();
                _hook = _hookGo.AddComponent<Hook>();
            }

            [Test]
            public void _x_50_y_50_returns_x_50_y_50_z_0()
            {
                _hook.SetPosition(new Vector2(50, 50));
                VectorAssert.AreEqual(new Vector3(50, 50, 0), _hookGo.transform.position, 0.0001f);
            }

            [Test]
            public void _x_negative_50_y_50_returns_x_negative_50_y_50_z_0()
            {
                _hook.SetPosition(new Vector2(-50, 50));
                VectorAssert.AreEqual(new Vector3(-50, 50, 0), _hookGo.transform.position, 0.0001f);
            }
        }

        public class GetRotation
        {
            private GameObject _hookGo;
            private Hook _hook;

            [SetUp]
            public void BeforeEach()
            {
                _hookGo = new GameObject();
                _hook = _hookGo.AddComponent<Hook>();
            }

            [Test]
            public void _x_0_y_0_z_0_returns_180()
            {
                _hookGo.transform.eulerAngles = new Vector3(0, 0, 0);
                var rotation = _hook.GetRotation();
                Assert.AreEqual(180, rotation, Mathf.Epsilon);
            }

            [Test]
            public void _x_0_y_0_z_180_returns_0()
            {
                _hookGo.transform.eulerAngles = new Vector3(0, 0, 180);
                var rotation = _hook.GetRotation();
                Assert.AreEqual(0, rotation, Mathf.Epsilon);
            }

            [Test]
            public void _x_0_y_0_z_negative_90_returns_270()
            {
                _hookGo.transform.eulerAngles = new Vector3(0, 0, -90);
                var rotation = _hook.GetRotation();
                Assert.AreEqual(270, rotation, Mathf.Epsilon);
            }

            [Test]
            public void _x_0_y_0_z_negative_90_returns_negative_90()
            {
                _hookGo.transform.eulerAngles = new Vector3(0, 0, -90);
                var rotation = _hook.GetRotation();
                Assert.AreEqual(270, rotation, Mathf.Epsilon);
            }

            [Test]
            public void _x_0_y_0_z_90_returns_90()
            {
                _hookGo.transform.eulerAngles = new Vector3(0, 0, 90);
                var rotation = _hook.GetRotation();
                Assert.AreEqual(90, rotation, Mathf.Epsilon);
            }
        }

        public class SetRotation
        {
            private GameObject _hookGo;
            private Hook _hook;

            [SetUp]
            public void BeforeEach()
            {
                _hookGo = new GameObject();
                _hook = _hookGo.AddComponent<Hook>();
            }

            [Test]
            public void _0_returns_x_0_y_0_z_180()
            {
                _hook.SetRotation(0);
                var expected = new Vector3(0, 0, 180);
                VectorAssert.AreEqual(_hookGo.transform.eulerAngles, expected, 0.0001f);
            }
            [Test]
            public void _90_returns_x_0_y_0_z_90()
            {
                _hook.SetRotation(90);
                var expected = new Vector3(0, 0, 90);
                VectorAssert.AreEqual(_hookGo.transform.eulerAngles, expected, 0.0001f);
            }
            [Test]
            public void _270_returns_x_0_y_0_z_270()
            {
                _hook.SetRotation(270);
                var expected = new Vector3(0, 0, 270);
                VectorAssert.AreEqual(_hookGo.transform.eulerAngles, expected, 0.0001f);
            }
            [Test]
            public void _180_returns_x_0_y_0_z_0()
            {
                _hook.SetRotation(180);
                var expected = new Vector3(0, 0, 0);
                VectorAssert.AreEqual(_hookGo.transform.eulerAngles, expected, 0.0001f);
            }
        }
    }
}