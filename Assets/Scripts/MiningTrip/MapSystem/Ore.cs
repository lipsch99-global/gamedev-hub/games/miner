﻿using MiningTrip.HookSystem;
using UnityEngine;

namespace MiningTrip.MapSystem
{
    public class Ore : MonoBehaviour, IVisitableMapObject
    {
        public void Accept(IMapObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
