﻿using MiningTrip.HookSystem;
using UnityEngine;

namespace MiningTrip.MapSystem
{
    public class Rock : MonoBehaviour, IVisitableMapObject
    {
        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawSphere(transform.position, transform.localScale.x / 2f);
        }

        public void Accept(IMapObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
