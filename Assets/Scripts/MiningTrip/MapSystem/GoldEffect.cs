﻿using System.Collections;
using UnityEngine;

namespace MiningTrip.MapSystem
{
    public class GoldEffect : MonoBehaviour
    {
        [SerializeField]
        private Transform EffectTransform = default;
        [SerializeField]
        private float Speed = 5f;
        [SerializeField]
        private float IdleTime = 0.75f;

        private void Start()
        {
            StartCoroutine(DoEffect());
        }

        private IEnumerator DoEffect()
        {
            float randomTime = Random.Range(IdleTime - IdleTime / 5f, IdleTime + IdleTime / 5f);
            yield return new WaitForSeconds(randomTime);
            while (true)
            {
                EffectTransform.localPosition += Vector3.right * Speed * Time.deltaTime;
                if (EffectTransform.localPosition.x >= 1)
                {
                    EffectTransform.localPosition = Vector3.left;
                    randomTime = Random.Range(IdleTime - IdleTime / 3f, IdleTime + IdleTime / 3f);
                    yield return new WaitForSeconds(randomTime);
                }
                else
                {
                    yield return null;
                }

            }
        }
    }
}
