﻿using Helper;
using UnityEngine;

namespace MiningTrip.MapSystem
{
    public class HitEffect : MonoBehaviour
    {
        private ParticleSystem oreParticleSystem;
        private Transform characterTransform;
        private Ore ore;
        private Rock rock;

        private void Start()
        {
            ore = GetComponentInParent<Ore>();
            rock = GetComponentInParent<Rock>();
            oreParticleSystem = GetComponent<ParticleSystem>();
            characterTransform = GameObject.FindGameObjectWithTag(Constants.Tags.CHARACTER).transform;
        }

        public void OnHookHitOre(Ore hitOre)
        {
            if (hitOre != ore)
            {
                return;
            }
            PlayEffect();
        }

        public void OnHookHitRock(Rock hitRock)
        {
            if (hitRock != rock)
            {
                return;
            }

            PlayEffect();
        }

        private void PlayEffect()
        {
            transform.rotation = HelperFunctions.LookAt(transform.parent.position, characterTransform.position);
            transform.position = transform.parent.position + (characterTransform.position - transform.position).normalized * transform.parent.localScale.x / 10f;
            oreParticleSystem.Play();
        }
    }
}
