﻿using UnityEngine;

namespace MiningTrip.MapSystem.Generator
{
    [CreateAssetMenu(fileName = "Mineral Type", menuName = "Map/Mineral Type")]
    public class MineralType : ScriptableObject
    {
        [SerializeField]
        private string _Name = default;
        public string Name
        {
            get
            {
                return _Name;
            }
        }
        [SerializeField]
        private GameObject _Prefab = default;
        public GameObject Prefab
        {
            get
            {
                return _Prefab;
            }
        }
        public enum OreOrRock
        {
            Ore, Rock
        }
        [SerializeField]
        private OreOrRock _Type = default;
        public OreOrRock Type
        {
            get
            {
                return _Type;
            }
        }
        [SerializeField]
        private int _HookLevel = default;
        public int HookLevel
        {
            get
            {
                return _HookLevel;
            }
        }
    }
}
