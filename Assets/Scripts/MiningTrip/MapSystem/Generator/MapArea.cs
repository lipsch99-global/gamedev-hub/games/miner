﻿using MiningTrip.CreatureSystem;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace MiningTrip.MapSystem.Generator
{
    [CreateAssetMenu(fileName = "Map Area", menuName = "Map/Area")]
    public class MapArea : ScriptableObject
    {
        [Header("Map Area")]
        [SerializeField]
        [Range(50, 150)]
        private int _Size = 100;
        public int Size
        {
            get
            {
                return _Size;
            }
        }
        [SerializeField]
        private int _PathAssuranceLevel = 0;
        public int PathAssuranceLevel
        {
            get
            {
                return _PathAssuranceLevel;
            }
        }
        [SerializeField]
        private TileBase _BackgroundTile = default;
        public TileBase BackgroundTile
        {
            get
            {
                return _BackgroundTile;
            }
        }
        [Header("Ores")]
        [SerializeField]
        private AreaMineral[] _Ores = default;
        public AreaMineral[] Ores
        {
            get
            {
                return _Ores;
            }
        }

        [Header("Rocks")]
        [SerializeField]
        private AreaMineral[] _Rocks = default;
        public AreaMineral[] Rocks
        {
            get
            {
                return _Rocks;
            }
        }

        [Header("Creatures")]
        [SerializeField]
        private AreaCreature[] _Creature = default;
        public AreaCreature[] Creature
        {
            get
            {
                return _Creature;
            }
        }
    }

    [System.Serializable]
    public class AreaMineral
    {
        [SerializeField]
        private MineralType _Type = default;
        public MineralType Type
        {
            get
            {
                return _Type;
            }
        }

        [SerializeField]
        private float _Probability = 1f;
        public float Probability
        {
            get
            {
                return _Probability;
            }
        }

        [SerializeField]
        [Range(0.2f,1f)]
        private float _MinSize = 1f;
        public float MinSize
        {
            get
            {
                return _MinSize;
            }
        }

        [SerializeField]
        [Range(1f, 3f)]
        private float _MaxSize = 1f;
        public float MaxSize
        {
            get
            {
                return _MaxSize;
            }
        }
    }

    [System.Serializable]
    public class AreaCreature
    {
        [SerializeField]
        private CreatureType _Type = default;
        public CreatureType Type
        {
            get
            {
                return _Type;
            }
        }

        [SerializeField]
        private float _Probability = 1f;
        public float Probability
        {
            get
            {
                return _Probability;
            }
        }
    }
}