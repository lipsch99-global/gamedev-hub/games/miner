﻿using System.Collections.Generic;
using System.Linq;
using Helper;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace MiningTrip.MapSystem.Generator
{
    public class MapGenerator
    {
        private MapArea[] mapAreas;
        private Tilemap tilemap;
        private Vector2Int upperLeftCoordinate;
        private int mapWidth;
        private Transform mapObjectRoot;

        private Dictionary<int, Dictionary<Vector2Int, MineralSpawnInfo>> spawnInfos;
        private const float hookWidth = 0.75f * 1.2f;
        private Vector2 characterSpawnPos = new Vector2(0, 6.48f);

        public MapGenerator(MapArea[] _mapAreas, Tilemap _tilemap, Vector2Int topLeft, int _mapWidth, Transform objectRoot)
        {
            mapAreas = _mapAreas;
            tilemap = _tilemap;
            upperLeftCoordinate = topLeft;
            mapWidth = _mapWidth;
            mapObjectRoot = objectRoot;

            if (tilemap.cellSize.x != 1 || tilemap.cellSize.y != 1)
            {
                throw new System.ArgumentOutOfRangeException("tilemap", "CellSize of Tilemap is not 1");
            }

            spawnInfos = new Dictionary<int, Dictionary<Vector2Int, MineralSpawnInfo>>();
        }

        public void Generate(int areaIndex)
        {
            spawnInfos[areaIndex] = new Dictionary<Vector2Int, MineralSpawnInfo>();
            DrawBackground(areaIndex);
            SpawnRocks(areaIndex);
            SpawnOres(areaIndex);
            DoPathAssurance(areaIndex);
            InstantiateMinerals(areaIndex);
            SpawnCreatures(areaIndex);
        }

        private void DrawBackground(int areaIndex)
        {
            int currentPos = upperLeftCoordinate.y - GetTilesFromTop(areaIndex);

            for (int y = 0; y < mapAreas[areaIndex].Size; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    tilemap.SetTile(new Vector3Int(x + upperLeftCoordinate.x, currentPos - y, 0),
                        mapAreas[areaIndex].BackgroundTile);
                }
            }
        }

        private void SpawnOres(int areaIndex)
        {
            SpawnMinerals(areaIndex, mapAreas[areaIndex].Ores);
        }
        private void SpawnRocks(int areaIndex)
        {
            SpawnMinerals(areaIndex, mapAreas[areaIndex].Rocks);
        }

        private void DoPathAssurance(int areaIndex)
        {
            MineralSpawnInfo[] mineralSpawnInfos = spawnInfos[areaIndex].Values.ToArray();
            MineralSpawnInfo[] oreSpawnInfos = mineralSpawnInfos.Where(x => x.MineralType.Type == MineralType.OreOrRock.Ore).ToArray();
            oreSpawnInfos = oreSpawnInfos.OrderByDescending(x => x.SpawnPosition.y).ToArray();

            for (int i = 0; i < oreSpawnInfos.Length; i++)
            {
                if (areaIndex != 0 && i == 0)
                {
                    continue;
                }

                Vector2 firstOrePos = i == 0 ? characterSpawnPos : oreSpawnInfos[i - 1].SpawnPosition;
                Vector2 secondOrePos = oreSpawnInfos[i].SpawnPosition;
                //Debug.DrawLine(firstOrePos, secondOrePos, Color.red, 300f);
                Vector2 dirFromFirstToSecond = secondOrePos - firstOrePos;
                float oreDistance = dirFromFirstToSecond.magnitude;
                dirFromFirstToSecond = dirFromFirstToSecond.normalized;

                for (float o = 0; o < oreDistance; o += 0.1f)
                {
                    Vector2 checkPosition = firstOrePos + dirFromFirstToSecond * o;
                    Vector2Int gridCheckPos = new Vector2Int(Mathf.FloorToInt(checkPosition.x),
                        Mathf.FloorToInt(checkPosition.y));
                    for (int y = -4; y < 4; y++)
                    {
                        for (int x = -4; x < 4; x++)
                        {
                            Vector2Int offset = new Vector2Int(x, y);
                            if (spawnInfos[areaIndex].ContainsKey(gridCheckPos + offset))
                            {
                                MineralSpawnInfo mineralToCheck = spawnInfos[areaIndex][gridCheckPos + offset];
                                if (mineralToCheck.MineralType.Type == MineralType.OreOrRock.Ore)
                                {
                                    continue;
                                }

                                float mineralToCheckDistance = Vector2.Distance(mineralToCheck.SpawnPosition, checkPosition);
                                float minDistance = hookWidth / 2f + mineralToCheck.SpawnSize / 2f;
                                if (mineralToCheckDistance < minDistance)
                                {
                                    //Debug.DrawLine(checkPosition, mineralToCheck.SpawnPosition, Color.blue, 300f);
                                    Vector2 dirToMove = Vector3.Cross(Vector3.forward, dirFromFirstToSecond).normalized;
                                    Vector2 closestPointOnLine = HelperFunctions.FindNearestPointOnLine(firstOrePos, dirFromFirstToSecond, mineralToCheck.SpawnPosition);
                                    Vector2 newPos = closestPointOnLine + dirToMove * minDistance;
                                    Vector2 altNewPos = closestPointOnLine - dirToMove * minDistance;
                                    //Debug.DrawLine(closestPointOnLine, newPos, Color.magenta, 300f);
                                    //Debug.DrawLine(closestPointOnLine, altNewPos, Color.green, 300f);

                                    float distanceMineralToNewPos = Vector2.Distance(mineralToCheck.SpawnPosition, newPos);
                                    float distanceMineralAltNewPos = Vector2.Distance(mineralToCheck.SpawnPosition, altNewPos);
                                    MineralSpawnInfo oppositeMineral = new MineralSpawnInfo(mineralToCheck);
                                    if (distanceMineralToNewPos > distanceMineralAltNewPos)
                                    {
                                        mineralToCheck.SetSpawnPos(altNewPos);
                                        oppositeMineral.SetSpawnPos(newPos);
                                    }
                                    else
                                    {
                                        mineralToCheck.SetSpawnPos(newPos);
                                        oppositeMineral.SetSpawnPos(altNewPos);
                                    }

                                    spawnInfos[areaIndex].Remove(gridCheckPos + offset);

                                    if (HasValidSpawn(areaIndex, oppositeMineral))
                                    {
                                        spawnInfos[areaIndex][oppositeMineral.GetGridPosition()] = oppositeMineral;
                                    }

                                    spawnInfos[areaIndex][mineralToCheck.GetGridPosition()] = mineralToCheck;
                                }
                            }
                        }
                    }
                }

            }
        }


        private MineralSpawnInfo PlaceRandomRock(AreaMineral mineral, int from, int to)
        {
            return new MineralSpawnInfo(mineral.Type, GetRandomSpawnPosition(from, to), Random.Range(mineral.MinSize, mineral.MaxSize));
        }


        private void SpawnMinerals(int areaIndex, AreaMineral[] areaMinerals)
        {
            foreach (var mineralType in areaMinerals)
            {
                int spawnAmount = Mathf.FloorToInt(mineralType.Probability);
                int probability = (int)((mineralType.Probability - spawnAmount) * 10);
                if (Random.Range(0, 10) <= probability)
                {
                    spawnAmount++;
                }

                float tilesPerOre = mapAreas[areaIndex].Size / spawnAmount;
                float maxOffset = tilesPerOre / 5f;
                float currentDepth = 0f;

                for (int i = 0; i < spawnAmount; i++)
                {
                    bool placed = false;
                    int tries = 0;
                    while (placed == false && tries < 30)
                    {
                        float yPos = Random.Range(GetMaxYPos(areaIndex) - currentDepth + maxOffset,
                            GetMaxYPos(areaIndex) - currentDepth - maxOffset);
                        Vector2 randomPos = new Vector2(GetRandomXPosition(), yPos);
                        float randomSize = Random.Range(mineralType.MinSize, mineralType.MaxSize);
                        MineralSpawnInfo newMineral = new MineralSpawnInfo(mineralType.Type, randomPos, randomSize);
                        if (HasValidSpawn(areaIndex, newMineral))
                        {
                            spawnInfos[areaIndex][newMineral.GetGridPosition()] = newMineral;
                            placed = true;
                        }
                        else
                        {
                            tries++;
                        }
                    }
                    currentDepth += tilesPerOre;
                }
            }
        }

        private void InstantiateMinerals(int areaIndex)
        {
            Transform root;
            if (mapObjectRoot.childCount >= areaIndex + 1)
            {
                root = mapObjectRoot.GetChild(areaIndex);
            }
            else
            {
                GameObject newGameObject = new GameObject("Area " + (areaIndex + 1));
                newGameObject.transform.parent = mapObjectRoot;
                root = newGameObject.transform;
            }

            foreach (var mineral in spawnInfos[areaIndex].Values)
            {
                GameObject newOre = GameObject.Instantiate(mineral.MineralType.Prefab, root);
                newOre.transform.position = mineral.SpawnPosition;
                newOre.transform.localScale = Vector3.one * mineral.SpawnSize;
            }

        }

        private void SpawnCreatures(int areaIndex)
        {
            foreach (var creatureType in mapAreas[areaIndex].Creature)
            {
                int spawnAmount = Mathf.FloorToInt(creatureType.Probability);
                int probability = (int)((creatureType.Probability - spawnAmount) * 10);
                if (Random.Range(0, 10) <= probability)
                {
                    spawnAmount++;
                }

                float tilesPerCreature = mapAreas[areaIndex].Size / spawnAmount;
                float maxOffset = tilesPerCreature / 5f;
                float currentDepth = 0f;

                for (int i = 0; i < spawnAmount; i++)
                {
                    float yPos = Random.Range(GetMaxYPos(areaIndex) - currentDepth + maxOffset,
                                     GetMaxYPos(areaIndex) - currentDepth - maxOffset) - 3;

                    Vector2 randomPos = new Vector2(GetRandomSide(), yPos);

                    GameObject newCreature = GameObject.Instantiate(creatureType.Type.Prefab, mapObjectRoot.GetChild(areaIndex));
                    newCreature.transform.position = randomPos;

                    if (randomPos.x < 0)
                    {
                        newCreature.transform.localScale = new Vector3(-1, 1, 1);
                    }


                    currentDepth += tilesPerCreature;
                }

            }
        }

        public int GetTilesFromTop(int areaIndex)
        {
            int sum = 0;
            for (int i = 0; i < areaIndex; i++)
            {
                sum += mapAreas[areaIndex].Size;
            }
            return sum;
        }

        private Vector3 GetRandomSpawnLocation(int areaIndex)
        {
            return new Vector3(GetRandomXPosition(), Random.Range(GetMinYPos(areaIndex), GetMaxYPos(areaIndex)), 0);
        }

        private float GetMaxYPos(int areaIndex)
        {
            return upperLeftCoordinate.y - GetTilesFromTop(areaIndex) - (areaIndex == 0 ? 2 : 0);
        }

        private float GetMinYPos(int areaIndex)
        {
            return upperLeftCoordinate.y - GetTilesFromTop(areaIndex) - mapAreas[areaIndex].Size + 2;
        }

        private float GetRandomXPosition()
        {
            float xMin = upperLeftCoordinate.x + 2;
            float xMax = upperLeftCoordinate.x + mapWidth - 2;

            return Random.Range(xMin, xMax);
        }

        private float GetRandomSide()
        {
            return Random.value > 0.5f ? 8 : -8;
        }


        private Vector2 GetRandomSpawnPosition(int yFrom, int yTo)
        {
            return new Vector2(GetRandomXPosition(), Random.Range((float)yFrom, (float)yTo));
        }

        private bool HasValidSpawn(int areaIndex, MineralSpawnInfo spawnInfo)
        {
            int FieldsToCheck = 2;
            if (spawnInfo.SpawnSize > 1.5f)
            {
                FieldsToCheck = 4;
            }
            Vector2Int gridPos = spawnInfo.GetGridPosition();

            for (int y = gridPos.y - FieldsToCheck; y <= gridPos.y + FieldsToCheck; y++)
            {
                for (int x = gridPos.x - FieldsToCheck; x <= gridPos.x + FieldsToCheck; x++)
                {
                    Vector2Int posToCheck = new Vector2Int(x, y);
                    if (spawnInfos[areaIndex].ContainsKey(posToCheck))
                    {
                        MineralSpawnInfo mineralToCheck = spawnInfos[areaIndex][posToCheck];
                        float distance = Vector2.Distance(mineralToCheck.SpawnPosition, spawnInfo.SpawnPosition);

                        float minDistance = mineralToCheck.SpawnSize / 2f + spawnInfo.SpawnSize / 2f + hookWidth;
                        if (distance < minDistance)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

    }

    public struct MineralSpawnInfo
    {
        public MineralType MineralType;
        public Vector2 SpawnPosition;
        public float SpawnSize;

        public MineralSpawnInfo(MineralType mineralType, Vector2 spawnPosition, float spawnSize)
        {
            MineralType = mineralType;
            SpawnPosition = spawnPosition;
            SpawnSize = spawnSize;
        }

        public MineralSpawnInfo(MineralSpawnInfo other)
        {
            MineralType = other.MineralType;
            SpawnPosition = other.SpawnPosition;
            SpawnSize = other.SpawnSize;
        }

        public Vector2Int GetGridPosition()
        {
            return new Vector2Int(Mathf.FloorToInt(SpawnPosition.x), Mathf.FloorToInt(SpawnPosition.y));
        }

        public void SetSpawnPos(Vector2 newSpawnPos)
        {
            SpawnPosition = newSpawnPos;
        }
    }
}