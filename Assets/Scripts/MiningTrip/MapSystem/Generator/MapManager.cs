﻿using UnityEngine;
using UnityEngine.Tilemaps;

namespace MiningTrip.MapSystem.Generator
{
    public class MapManager : MonoBehaviour
    {
        [SerializeField]
        private Transform character = default;
        [SerializeField]
        private MapArea[] mapAreas = default;
        [SerializeField]
        private Tilemap tilemap = default;
        [SerializeField]
        private Vector2Int topLeft = default;
        [SerializeField]
        private int mapWidth = default;
        [SerializeField]
        private Transform mapObjectRoot = default;

        private MapGenerator mapGenerator;

        private int currentArea = 0;

        private void Start()
        {
            mapGenerator = new MapGenerator(mapAreas, tilemap, topLeft, mapWidth, mapObjectRoot);
            mapGenerator.Generate(0);
        }

        private void Update()
        {
            if (character.position.y < topLeft.y - mapGenerator.GetTilesFromTop(currentArea) - mapAreas[currentArea].Size + 20)
            {
                GoToNextArea();
            }
        }

        private void GoToNextArea()
        {
            currentArea++;
            mapGenerator.Generate(currentArea);
        }
    }
}
