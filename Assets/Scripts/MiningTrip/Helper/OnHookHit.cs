﻿using UnityEngine;

namespace MiningTrip.Helper
{
    public interface OnHookHit
    {
        void OnHookHit(Vector2 dir);
    }
}
