﻿using Helper;
using UnityEngine;

namespace MiningTrip.Helper
{
    public class PlayOnHookHit : MonoBehaviour, OnHookHit
    {
        [SerializeField]
        private Transform effectTransform = default;
        private ParticleSystem oreParticleSystem;

        private void Start()
        {
            oreParticleSystem = effectTransform.GetComponent<ParticleSystem>();
        }

        public void OnHookHit(Vector2 dir)
        {
            effectTransform.rotation = HelperFunctions.LookAt(effectTransform.position, dir.Add(transform.position));
            oreParticleSystem.Play();
        }
    }
}
