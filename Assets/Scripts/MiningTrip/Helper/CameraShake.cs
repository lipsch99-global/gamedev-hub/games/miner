﻿using System.Collections;
using Cinemachine;
using MiningTrip.CreatureSystem;
using MiningTrip.MapSystem;
using UnityEngine;

namespace MiningTrip.Helper
{
    public class CameraShake : MonoBehaviour
    {
        [SerializeField]
        private CinemachineVirtualCamera cam = default;

        [SerializeField]
        private float amplitudeGain = 1f;
        [SerializeField]
        private float shakeIntensity = 5f;
        [SerializeField]
        private float shakeDuration = 0.5f;

        private CinemachineBasicMultiChannelPerlin noise;

        void Start()
        {
            noise = cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }


        public void OnHitOre(Ore ore)
        {
            StartCoroutine(ProcessShake(amplitudeGain / 2f, shakeIntensity / 2f,shakeDuration));
        }
        public void OnHitRock(Rock rock)
        {
            StartCoroutine(ProcessShake(amplitudeGain, shakeIntensity, shakeDuration));
        }

        public void OnHitCreature(Creature creature)
        {
            StartCoroutine(ProcessShake(amplitudeGain, shakeIntensity, shakeDuration));
        }

        public void OnHitBorder()
        {
            StartCoroutine(ProcessShake(amplitudeGain, shakeIntensity, shakeDuration));
        }

        private IEnumerator ProcessShake(float amplitudeGain, float shakeIntensity, float shakeDuration)
        {
            Noise(amplitudeGain, shakeIntensity);
            yield return new WaitForSeconds(shakeDuration);
            Noise(0, 0);
        }

        public void Noise(float amplitudeGain, float frequencyGain)
        {
            noise.m_AmplitudeGain = amplitudeGain;
            noise.m_FrequencyGain = frequencyGain;
        }
    }
}
