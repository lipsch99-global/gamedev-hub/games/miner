﻿using UnityEngine;

namespace MiningTrip.HookSystem.Behaviour
{
    public abstract class HookBehaviour : ScriptableObject
    {
        public enum UpdatePattern //fix naming
        {
            Position,
            Rotation,
            PositionAndRotation
        }

        [SerializeField] private UpdatePattern updateType = default;
        public UpdatePattern UpdateType => updateType;

        public virtual State GetNewStateInstance()
        {
            return new State();
        }

        public virtual Vector2 GetNewPosition(ref State state, Hook.Hook hook)
        {
            throw new System.NotImplementedException();
        }

        public virtual float GetNewRotation(ref State state, Hook.Hook hook)
        {
            throw new System.NotImplementedException();
        }

        public virtual bool IsBehaviourFinished(State state, Hook.Hook hook)
        {
            return false;
        }
    }
}