﻿using System;
using Helper;
using UnityEngine;

namespace MiningTrip.HookSystem.Behaviour
{
    [CreateAssetMenu(menuName = "Hook Behaviour/HookRotator")]
    public class HookRotator : HookBehaviour
    {
        public override float GetNewRotation(ref State state, Hook.Hook hook)
        {
            Vector2 directionToCharacter =
                ((CharacterState) state).CharacterTransform.position.Add(-hook.GetPosition());

            float newRotation = HelperFunctions.GetRotationFromDirection(directionToCharacter) - 180f;
            return HelperFunctions.ClampRotation(newRotation);
        }

        public override State GetNewStateInstance()
        {
            return new CharacterState();
        }
    }
}