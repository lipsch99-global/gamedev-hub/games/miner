﻿using Helper;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace MiningTrip.HookSystem.Behaviour
{
    [CreateAssetMenu(menuName = "Hook Behaviour/Retract Hook")]
    public class RetractHook : HookBehaviour
    {
        [SerializeField] private float retractSpeed = 3f;
        [SerializeField] private float targetDistanceFromCharacter = 2f;

        public override Vector2 GetNewPosition(ref State state, Hook.Hook hook)
        {
            var dir = HelperFunctions.GetDirectionFromRotation(hook.GetRotation());
            dir *= -1;
            var deltaTime = (Time.time - state.LastExecution);
            var newPos = hook.GetPosition() + deltaTime * retractSpeed * dir;
            return newPos;
        }

        public override State GetNewStateInstance()
        {
            return new CharacterState();
        }
        
        public override bool IsBehaviourFinished(State state, Hook.Hook hook)
        {
            return (Vector3.Distance(hook.GetPosition(), ((CharacterState) state).CharacterTransform.position) <
                    targetDistanceFromCharacter);
        }
    }
}