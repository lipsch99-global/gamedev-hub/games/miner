﻿using Helper;
using UnityEditor;
using UnityEngine;

namespace MiningTrip.HookSystem.Behaviour
{
    [CreateAssetMenu(menuName = "Hook Behaviour/Standard Shoot")]
    public class StandardShoot : HookBehaviour
    {
        [SerializeField] private float shootSpeed = 3f;

        public override Vector2 GetNewPosition(ref State state, Hook.Hook hook)
        {
            var dir = HelperFunctions.GetDirectionFromRotation(hook.GetRotation());
            var deltaTime = (Time.time - state.LastExecution);
            var newPos = hook.GetPosition() + deltaTime * shootSpeed * dir;
            return newPos;
        }

        public override float GetNewRotation(ref State state, Hook.Hook hook)
        {
            throw new System.NotImplementedException();
        }
    }
}