﻿using Helper;
using UnityEngine;

namespace MiningTrip.HookSystem.Behaviour
{
    [CreateAssetMenu(menuName = "Hook Behaviour/Standard Swing")]
    public class StandardSwing : HookBehaviour
    {
        [SerializeField] private float swingSpeed = default;
        [SerializeField] private float swingDistance = default;
        [SerializeField] private float swingRange = default;

        public override Vector2 GetNewPosition(ref State state, Hook.Hook hook)
        {
            float baseAngle = 1.5f * Mathf.PI; // 270 degrees
            float halfAngleRange = (swingRange / 2f) * Mathf.Deg2Rad;
            float c = Mathf.Cos(swingSpeed * state.LastExecution);
            float angle = halfAngleRange * c + baseAngle;
            Vector2 newPos = new Vector2(swingDistance * Mathf.Cos(angle), swingDistance * Mathf.Sin(angle));
            newPos = newPos.Add(((CharacterState) state).CharacterTransform.position);
            return newPos;
        }

        public override State GetNewStateInstance()
        {
            return new CharacterState();
        }
    }
}