﻿using UnityEngine;

namespace MiningTrip.HookSystem
{
    public class CharacterState : State
    {
        public readonly Transform CharacterTransform;

        public CharacterState()
        {
            //TODO: Optimize this. Dependency to Character component not good
            CharacterTransform = UnityEngine.Object.FindObjectOfType<Character>().transform;
        }
    }
}    