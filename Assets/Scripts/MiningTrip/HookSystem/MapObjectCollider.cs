﻿using System;
using Cinemachine;
using MiningTrip.MapSystem;
using UnityEngine;

namespace MiningTrip.HookSystem
{
    [RequireComponent(typeof(Collider2D), typeof(Rigidbody2D))]
    public class MapObjectCollider : MonoBehaviour
    {
        [Tooltip("Of Type IMapObjectVisitor")] [SerializeField]
        private Component visitorComponent = null;

        private IMapObjectVisitor _visitor;
        private bool _running = false;

        private void Start()
        {
            _visitor = visitorComponent.GetComponent<IMapObjectVisitor>();
        }

        public void SetIsRunningState(bool newState)
        {
            _running = newState;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!_running) return;
            var visitable = other.GetComponent<IVisitableMapObject>();
            visitable?.Accept(_visitor);
        }

        private void OnValidate()
        {
            if (visitorComponent == null)
            {
                return;
            }

            if (visitorComponent.GetComponent<IMapObjectVisitor>() == null)
            {
                throw new ArgumentException("visitor is not of type IMapObjectVisitor");
            }
        }
    }
}