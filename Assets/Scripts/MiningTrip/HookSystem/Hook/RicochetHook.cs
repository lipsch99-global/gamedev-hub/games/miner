﻿using Helper;
using UnityEngine;

namespace MiningTrip.HookSystem.Hook
{
    public class RicochetHook : Hook
    {
        public override void Visit(MapBorder border)
        {
            AttachedRope.RemoveAlignment();
            AttachedRope = ropeFactory.GetInstance(transform.parent);
            AttachedRope.SetAlignment(GetPosition(), transform);
            var rotation = GetRotation();
            var dir = HelperFunctions.GetDirectionFromRotation(rotation);
            dir.x *= -1f;
            var newRotation = HelperFunctions.GetRotationFromDirection(dir);
            SetRotation(newRotation);
            Shoot();
        }
    }
}