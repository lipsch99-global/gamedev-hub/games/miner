﻿using Global.GameEventSystem.Base;
using Helper;
using MiningTrip.HookSystem.Behaviour;
using MiningTrip.MapSystem;
using UnityEngine;

namespace MiningTrip.HookSystem.Hook
{
    public class Hook : MonoBehaviour, IMapObjectVisitor
    {
        [SerializeField] protected HookBehaviour swingBehaviour = default;
        [SerializeField] protected HookBehaviour shootBehaviour = default;
        [SerializeField] protected HookBehaviour rotationBehaviour = default;
        [SerializeField] protected HookBehaviour retractHookBehaviour = default;
        [SerializeField] protected Executor behaviourExecutor = default;
        [SerializeField] protected Executor rotationBehaviourExecutor = default;
        [SerializeField] protected RopeFactory ropeFactory = default;
        [SerializeField] protected MapObjectCollider mapObjectCollider = default;
        [SerializeField] protected HookEvents hookEvents = default;
    
        protected Character Character;
        protected Rope AttachedRope = null;

        protected virtual void Start()
        {
            Character = FindObjectOfType<Character>();
            AttachedRope = ropeFactory.GetInstance(transform.parent); //TODO: Parent? Sure$
            AttachedRope.SetAlignment(Character.transform, transform);
            Swing();
        }

        protected virtual void Swing()
        {
            mapObjectCollider.SetIsRunningState(false);
            behaviourExecutor.Init(swingBehaviour, this);
            rotationBehaviourExecutor.Init(rotationBehaviour, this);
            behaviourExecutor.Run();
            rotationBehaviourExecutor.Run();
        }

        public virtual void Shoot()
        {
            mapObjectCollider.SetIsRunningState(true);
            behaviourExecutor.Stop();
            rotationBehaviourExecutor.Stop();
            behaviourExecutor.Init(shootBehaviour, this);
            behaviourExecutor.Run();
        }

        public virtual float GetRotation()
        {
            var rotation = transform.rotation.eulerAngles.z;
            rotation += 180;
            rotation += (180 - rotation) * 2;

            rotation = HelperFunctions.ClampRotation(rotation);

            return rotation;
        }

        public virtual Vector2 GetPosition()
        {
            return transform.position;
        }

        public virtual void SetRotation(float newRotation)
        {
            newRotation += 180;
            newRotation *= -1f;

            transform.eulerAngles = new Vector3(0, 0, newRotation);
        }

        public virtual void SetPosition(Vector2 newPosition)
        {
            transform.position = newPosition;
        }

        protected virtual void HookRetracted()
        {
            behaviourExecutor.behaviourFinished -= Swing;
            Swing();
        }

        protected virtual void CharacterReachedHook()
        {
            Character.CharacterReachedHook -= CharacterReachedHook;
            AttachedRope = ropeFactory.GetInstance(transform.parent); //TODO: Parent? Sure?
            AttachedRope.SetAlignment(Character.transform, transform);
            Swing();
        }

        public virtual void Visit(Ore ore)
        {
            behaviourExecutor.Stop();
            SetPosition(ore.transform.position);
            AttachedRope.AlignToAlignment();
            AttachedRope.RemoveAlignment();
            AttachedRope = null;
            Character.CharacterReachedHook += CharacterReachedHook;
            Character.MoveTo(GetPosition());
        }

        public virtual void Visit(Rock rock)
        {
            //TODO: Lose Hook

            behaviourExecutor.Stop();
            behaviourExecutor.Init(retractHookBehaviour, this);
            behaviourExecutor.Run();
            behaviourExecutor.behaviourFinished += HookRetracted;
        }

        public virtual void Visit(MapBorder border)
        {
            behaviourExecutor.Stop();
            behaviourExecutor.Init(retractHookBehaviour, this);
            behaviourExecutor.Run();
            behaviourExecutor.behaviourFinished += HookRetracted;
        }
    }
}