﻿//using Global.GameEventSystem.Events;

using Global.GameEventSystem.Events;

namespace MiningTrip.HookSystem.Hook
{
    [System.Serializable]
    public class HookEvents
    {
        public GameEventRock RockHit;
        public GameEventOre OreHit;
        public GameEventCreature CreatureHit;
        public GameEventMapBorder MapBorderHit;
    }    
}