﻿using UnityEngine;

namespace MiningTrip.HookSystem
{
    public class MapBorder : MonoBehaviour, IVisitableMapObject
    {
        public void Accept(IMapObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}