﻿
using UnityEngine;

namespace MiningTrip.HookSystem
{
    [CreateAssetMenu(menuName = "Factory/Rope Factory")]
    public class RopeFactory : ScriptableObject
    {
        [SerializeField] private Rope ropePrefab = default;

        public Rope GetInstance(Transform parent)
        {
            return GameObject.Instantiate(ropePrefab, parent);
        }
    }
}