﻿using System;
using MiningTrip.HookSystem.Behaviour;
using UnityEngine;
using UnityEngine.Serialization;

namespace MiningTrip.HookSystem
{
    public class Executor : MonoBehaviour
    {
        public Action behaviourFinished;
        private State _state;
        private HookBehaviour _behaviour;
        private Hook.Hook _hook;

        private bool _running = false;

        public void Init(HookBehaviour behaviour, Hook.Hook hook)
        {
            _behaviour = behaviour;
            _state = behaviour.GetNewStateInstance();
            _hook = hook;
        }

        public void Run()
        {
            _running = true;
        }

        public void Stop()
        {
            _running = false;
        }

        private void Update()
        {
            if (_running == false)
                return;

            if (_behaviour.UpdateType == HookBehaviour.UpdatePattern.Position ||
                _behaviour.UpdateType == HookBehaviour.UpdatePattern.PositionAndRotation)
                _hook.SetPosition(_behaviour.GetNewPosition(ref _state, _hook));

            if (_behaviour.UpdateType == HookBehaviour.UpdatePattern.Rotation ||
                _behaviour.UpdateType == HookBehaviour.UpdatePattern.PositionAndRotation)
                _hook.SetRotation(_behaviour.GetNewRotation(ref _state, _hook));

            if (_behaviour.IsBehaviourFinished(_state, _hook))
            {
                _running = false;
                behaviourFinished?.Invoke();
            }
            
            _state.LastExecution = Time.time;
        }
    }
}