﻿using System;
using Helper;
using UnityEngine;
using UnityEngine.Serialization;

namespace MiningTrip.HookSystem
{
    public class Rope : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer ropeRenderer = default;

        private bool _alignmentSet = false;
        private Transform _alignmentA = null;
        private Vector2 _altAlignmentA;
        private Transform _alignmentB = null;


        public void Align(Vector2 a, Vector2 b)
        {
            //position
            transform.position = Vector2.Lerp(a, b, 0.5f);
            //rotation
            Vector2 dir = a.Subtract(transform.position).normalized;
            Vector3 rotation = new Vector3(0, 0, HelperFunctions.GetRotationFromDirection(dir) * -1f);
            transform.rotation = Quaternion.Euler(rotation);
            //length
            ropeRenderer.size = new Vector2(1, Vector2.Distance(a, b));
        }

        public void SetAlignment(Transform a, Transform b)
        {
            _alignmentA = a;
            _alignmentB = b;
            _alignmentSet = true;
        }

        public void SetAlignment(Vector2 a, Transform b)
        {
            _altAlignmentA = a;
            _alignmentB = b;
            _alignmentSet = true;
        }

        public void RemoveAlignment()
        {
            _alignmentA = null;
            _alignmentB = null;
            _alignmentSet = false;
        }

        public void AlignToAlignment()
        {
            if (_alignmentA != null)
            {
                Align(_alignmentA.position, _alignmentB.position);
            }
            else
            {
                Align(_altAlignmentA, _alignmentB.position);
            }
        }

        private void Update()
        {
            if (_alignmentSet)
            {
                AlignToAlignment();
            }
        }
    }
}