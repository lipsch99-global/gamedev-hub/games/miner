﻿using MiningTrip.MapSystem;

namespace MiningTrip.HookSystem
{
    public interface IMapObjectVisitor
    {
        void Visit(Ore ore);
        void Visit(Rock rock);
        void Visit(MapBorder border);
    }

    public interface IVisitableMapObject
    {
        void Accept(IMapObjectVisitor visitor);
    }
}