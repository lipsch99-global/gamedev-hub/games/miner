﻿using Helper;
using UnityEngine;

namespace MiningTrip.CreatureSystem
{
    public class Mole : Creature
    {
        [Header("Mole")]
        [SerializeField]
        private float moveSpeed = 3f;
        [SerializeField]
        private float rotateSpeed = 1f;


        protected override void Update()
        {
            base.Update();

            if (Vector2.Distance(transform.position, targetPosition) < 0.5f || isAggressive)
            {
                base.GetNewTargetPosition();
            }

            Move();
        }

        protected override void Move()
        {
            //Vector2 dir = targetPosition.Subtract(transform.position).normalized;
            Quaternion targetRotation = HelperFunctions.LookAt(transform.position,targetPosition);

            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
            transform.position = transform.position + transform.up * moveSpeed * Time.deltaTime;
        }

        protected override Vector2 GetPosition()
        {
            return transform.position;
        }

        protected override void Turn()
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, 1, 1);
        }
    }
}
