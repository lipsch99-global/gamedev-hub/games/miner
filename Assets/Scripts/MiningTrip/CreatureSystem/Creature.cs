﻿using Helper;
using MiningTrip.CreatureSystem.MovementBehaviours;
using MiningTrip.MapSystem;
using UnityEngine;

namespace MiningTrip.CreatureSystem
{
    public abstract class Creature : MonoBehaviour
    {
        [Header("Creature")]
        [SerializeField]
        private CreatureMovementBehaviour passiveMovementBehaviour = default;
        [SerializeField]
        private CreatureMovementBehaviour aggressiveMovementBehaviour = default;
        [SerializeField]
        private bool hasSenseRange = false;
        [SerializeField]
        private float senseRange = 5f;

        protected bool isAggressive;
        protected int moveDir;
        protected Vector2 targetPosition;

        private Transform characterTransform;

        protected virtual void Start()
        {
            characterTransform = GameObject.FindGameObjectWithTag(Constants.Tags.CHARACTER).transform;
            moveDir = GetPosition().x > 0 ? -1 : 1;
            GetNewTargetPosition();
        }

        protected virtual void Update()
        {
            if (moveDir > 0)
            {
                if (GetPosition().x > 6)
                {
                    moveDir = -1;
                    Turn();
                    GetNewTargetPosition();
                }
            }
            else
            {
                if (GetPosition().x < -6)
                {
                    moveDir = 1;
                    Turn();
                    GetNewTargetPosition();
                }
            }
        }

        protected void GetNewTargetPosition()
        {
            targetPosition = (isAggressive ? aggressiveMovementBehaviour : passiveMovementBehaviour)
                .GetPositionAfterMovement(GetPosition(), moveDir, characterTransform.position);
        }

        public virtual void OnHookHitCreature(Creature creature)
        {
            if (creature != this)
            {
                PerformSenseRange(creature.transform.position);
                return;
            }
            GetAggressive();
        }

        public virtual void OnHookHitRock(Rock rock)
        {
            PerformSenseRange(rock.transform.position);
        }

        public virtual void OnHookHitOre(Ore ore)
        {
            PerformSenseRange(ore.transform.position);
        }

        private void PerformSenseRange(Vector2 hitPos)
        {
            if (hasSenseRange == false)
            {
                return;
            }
            if (Vector2.Distance(hitPos, GetPosition()) <= senseRange)
            {
                GetAggressive();
            }
        }

        private void GetAggressive()
        {
            isAggressive = true;
            GetNewTargetPosition();
        }

        protected abstract void Move();

        protected abstract Vector2 GetPosition();

        protected abstract void Turn();

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(targetPosition, 0.5f);
        }

        public void Kill()
        {
            Destroy(gameObject);
        }
    }
}
