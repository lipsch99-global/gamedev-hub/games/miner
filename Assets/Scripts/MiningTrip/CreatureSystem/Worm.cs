﻿using Helper;
using UnityEngine;

namespace MiningTrip.CreatureSystem
{
    public class Worm : Creature
    {
        [Header("Worm")]
        [SerializeField]
        private Transform headTransform = default;
        [SerializeField]
        private Transform[] bodyLimbTransforms = default;
        [SerializeField]
        private float moveSpeed = 3f;
        [SerializeField]
        private float rotateSpeed = 3f;


        private float[] bodyLimbsDistances;

        protected override void Start()
        {
            base.Start();

            bodyLimbsDistances = new float[bodyLimbTransforms.Length];
            for (int i = 0; i < bodyLimbTransforms.Length; i++)
            {
                if (i == 0)
                {
                    bodyLimbsDistances[i] = Vector2.Distance(headTransform.position, bodyLimbTransforms[i].position);
                }
                else
                {
                    bodyLimbsDistances[i] = Vector2.Distance(bodyLimbTransforms[i - 1].position, bodyLimbTransforms[i].position);
                }
            }
        }

        protected override void Update()
        {
            base.Update();

            if (Vector2.Distance(headTransform.position, targetPosition) < 0.5f || isAggressive)
            {
                base.GetNewTargetPosition();
            }

            Move();
        }

        protected override void Move()
        {
            Vector2 moveDir = headTransform.up * moveSpeed * Time.deltaTime;
            Vector2 newPos = moveDir.Add(headTransform.position);
            headTransform.position = newPos;
            Quaternion targetRotation = HelperFunctions.LookAt(headTransform.position, targetPosition);
            Quaternion newRotation = Quaternion.Lerp(headTransform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
            headTransform.rotation = newRotation;

            for (int i = 0; i < bodyLimbTransforms.Length; i++)
            {
                if (i == 0)
                {
                    SetLimbRotation(headTransform, bodyLimbTransforms[i], bodyLimbTransforms[i + 1]);
                    SetLimbPosition(headTransform, bodyLimbTransforms[i], bodyLimbsDistances[i]);
                }
                else if (i == bodyLimbTransforms.Length - 1)
                {
                    SetLimbRotation(bodyLimbTransforms[i - 1], bodyLimbTransforms[i], null);
                    SetLimbPosition(bodyLimbTransforms[i - 1], bodyLimbTransforms[i], bodyLimbsDistances[i]);
                }
                else
                {
                    SetLimbRotation(bodyLimbTransforms[i - 1], bodyLimbTransforms[i], bodyLimbTransforms[i + 1]);
                    SetLimbPosition(bodyLimbTransforms[i - 1], bodyLimbTransforms[i], bodyLimbsDistances[i]);
                }
            }
        }

        private void SetLimbRotation(Transform before, Transform activeLimb, Transform after)
        {
            Quaternion activeToBefore = HelperFunctions.LookAt(activeLimb.position, before.position);
            if (after == null)
            {
                activeLimb.rotation = activeToBefore;
            }
            else
            {
                Quaternion afterToActive = HelperFunctions.LookAt(after.position, activeLimb.position);
                activeLimb.rotation = Quaternion.Lerp(activeToBefore, afterToActive, 0.5f);
            }
        }
        private void SetLimbPosition(Transform before, Transform activeLimb, float distance)
        {
            activeLimb.position = (activeLimb.position - before.position).normalized * distance + before.position;
        }

        protected override Vector2 GetPosition()
        {
            return headTransform.position;
        }

        protected override void Turn()
        {
            headTransform.localScale = new Vector3(headTransform.localScale.x * -1, 1, 1);
        }
    }
}
