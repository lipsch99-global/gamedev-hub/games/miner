﻿using UnityEngine;

namespace MiningTrip.CreatureSystem
{
    [CreateAssetMenu(fileName = "Creature Type", menuName = "Map/Creature Type")]
    public class CreatureType : ScriptableObject
    {
        [SerializeField]
        private string _Name = default;
        public string Name
        {
            get
            {
                return _Name;
            }
        }
        [SerializeField]
        private GameObject _Prefab = default;
        public GameObject Prefab
        {
            get
            {
                return _Prefab;
            }
        }
    }
}
