﻿using UnityEngine;

namespace MiningTrip.CreatureSystem.MovementBehaviours
{
    public abstract class CreatureMovementBehaviour : ScriptableObject
    {
        public abstract Vector2 GetPositionAfterMovement(Vector2 oldPosition, int horizontalDir, Vector2 characterPosition);
    }
}
