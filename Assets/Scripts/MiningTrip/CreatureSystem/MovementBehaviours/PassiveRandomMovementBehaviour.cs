﻿using UnityEngine;

namespace MiningTrip.CreatureSystem.MovementBehaviours
{
    [CreateAssetMenu(fileName = "New Behaviour", menuName = "Movement Behaviours/Passive Random")]
    public class PassiveRandomMovementBehaviour : CreatureMovementBehaviour
    {
        public override Vector2 GetPositionAfterMovement(Vector2 oldPosition, int horizontalDir, Vector2 characterPosition)
        {
            return oldPosition + (new Vector2(Mathf.Clamp(horizontalDir, -1, 1), Random.Range(-1, 1))).normalized * 2f;
        }
    }
}
