﻿using UnityEngine;

namespace MiningTrip.CreatureSystem.MovementBehaviours
{
    [CreateAssetMenu(fileName = "New Behaviour", menuName = "Movement Behaviours/Aggressive Straight")]
    public class AggressiveStraightMovementBehaviour : CreatureMovementBehaviour
    {
        public override Vector2 GetPositionAfterMovement(Vector2 oldPosition, int horizontalDir, Vector2 characterPosition)
        {
            return characterPosition;
        }
    }
}
