﻿using UnityEngine;
using UnityEngine.UI;

namespace MiningTrip.Managers
{
    public class EndCardManager : MonoBehaviour
    {
        [SerializeField]
        private Text goldAmountText = default;

        public void SetValues(int goldAmount)
        {
            goldAmountText.text = goldAmount.ToString();
        }
    }
}
