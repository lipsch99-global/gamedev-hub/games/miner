﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Global;
using Global.GameEventSystem.Base;
using Global.GameEventSystem.Events;
//using Global.GameEventSystem.Events;
using Helper;
using MiningTrip.CreatureSystem;
using MiningTrip.MapSystem;
using UnityEngine;

namespace MiningTrip.Managers
{
    public class MiningTripManager : MonoBehaviour
    {
        [SerializeField]
        private int initialHookAmount = 3;

        [SerializeField]
        private GameEvent _TripEnded;
        public GameEvent TripEnded
        {
            get
            {
                return _TripEnded;
            }
            set
            {
                _TripEnded = value;
            }
        }
        [SerializeField]
        private GameEventInt _HookAmountChanged;
        public GameEventInt HookAmountChanged
        {
            get
            {
                return _HookAmountChanged;
            }
            set
            {
                _HookAmountChanged = value;
            }
        }

        private int _HookAmount;
        public int HookAmount
        {
            get
            {
                return _HookAmount;
            }
            private set
            {
                _HookAmount = value;
                HookAmountChanged?.Raise(value);
                if (HookAmount < 1 && GameRunning)
                {
                    LoseGame();
                }
            }
        }
        public bool GameRunning { get; private set; }

        private Character character;
        private List<Ore> hookedOres = new List<Ore>();
        private Vector2 startPos;
        private EndCardManager endCardManager;

        // Start is called before the first frame update
        private void Start()
        {
            GameRunning = true;
            HookAmount = initialHookAmount;
            character = GameObject.FindGameObjectWithTag(Constants.Tags.CHARACTER).GetComponent<Character>();
            startPos = character.transform.position;
            endCardManager = FindObjectOfType<EndCardManager>();
        }



        private void LoseGame()
        {
            GameRunning = false;
            StartCoroutine(MoveBackUpRope());
            TripEnded?.Raise();

            int oreAmount = hookedOres.Count();
            endCardManager.SetValues(oreAmount);
            GameManager.Instance.GoldAmount += oreAmount;
        }

        private IEnumerator MoveBackUpRope()
        {
            foreach (var ore in hookedOres)
            {
                Vector2 dirToOre = (ore.transform.position - character.transform.position).normalized;
                while (Vector2.Distance(ore.transform.position, character.transform.position) > 0.2f)
                {
                    character.transform.position = character.transform.position.Add(dirToOre * Time.deltaTime * 5f);
                    yield return null;
                }
            }
            Vector2 dirToStart = startPos.Subtract(character.transform.position).normalized;
            while (Vector2.Distance(startPos, character.transform.position) > 0.2f)
            {
                character.transform.position = character.transform.position.Add(dirToStart * Time.deltaTime * 5f);
                yield return null;
            }
        }

        public void HookHitRock(Rock rock)
        {
            HookAmount--;
        }

        public void HookHitCreature(Creature creature)
        {
            HookAmount--;
        }

        public void CreatureHitCharacter(Creature creature)
        {
            creature.Kill();
            HookAmount--;
        }

        public void HookHitOre(Ore ore)
        {
            hookedOres.Insert(0, ore);
        }

        public void EndTrip()
        {
            //save progress
            SceneController.Instance.LoadMenuScene();
        }
    }
}
