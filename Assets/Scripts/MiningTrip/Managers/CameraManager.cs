﻿using Cinemachine;
using UnityEngine;

namespace MiningTrip.Managers
{
    public class CameraManager : MonoBehaviour
    {
        [SerializeField]
        private CinemachineVirtualCamera downCamera = default;
        [SerializeField]
        private CinemachineVirtualCamera upCamera = default;

        private CinemachineBrain cinemachineBrain;
        private Transform character;

        void Start()
        {
            cinemachineBrain = GetComponent<CinemachineBrain>();
            character = FindObjectOfType<Character>().transform;
        }

        public void OnTripEnded()
        {
            downCamera.Priority = 0;
            upCamera.Priority = 1;
        }
    
    }
}
