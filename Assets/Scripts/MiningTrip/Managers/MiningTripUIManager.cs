﻿using Global;
using UnityEngine;
using UnityEngine.UI;

namespace MiningTrip.Managers
{
    public class MiningTripUIManager : MonoBehaviour
    {
        [SerializeField]
        private Image[] hookImages = default;
        [SerializeField]
        private GameObject topBarRoot = default;
        [SerializeField]
        private GameObject optionsRoot = default;
        [SerializeField]
        private GameObject endCardRoot = default;

        private MiningTripManager tripManager;
        private Transform camTransform;

        public bool MenuOpened
        {
            get
            {
                return optionsRoot.activeSelf;
            }
        }

        private void Start()
        {
            tripManager = FindObjectOfType<MiningTripManager>();
            camTransform = Camera.main.transform;
        }

        private void Update()
        {
            if (tripManager.GameRunning == false && endCardRoot.activeSelf == false)
            {
                if (camTransform.position.y > 10.8f)
                {
                    OpenEndCard();
                }
            }
        }


        public void HookAmountChanged(int newAmount)
        {
            for (int i = 0; i < hookImages.Length; i++)
            {
                hookImages[i].enabled = (i < newAmount);
            }
        }

        public void OnOpenOptions()
        {
            optionsRoot.SetActive(true);
        }

        public void OnCloseOptions()
        {
            optionsRoot.SetActive(false);
        }

        public void OnGoToMenu()
        {
            SceneController.Instance.LoadMenuScene();
        }

        public void OpenEndCard()
        {
            topBarRoot.SetActive(false);
            endCardRoot.SetActive(true);
        }

        public void OnEndTrip()
        {
            tripManager.EndTrip();
        }
    }
}
