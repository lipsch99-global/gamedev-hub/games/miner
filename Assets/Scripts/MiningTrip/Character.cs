﻿using System;
//using Global.GameEventSystem.Events;
using Helper;
using MiningTrip.CreatureSystem;
using MiningTrip.HookSystem;
using UnityEngine;
using UnityEngine.UI;

namespace MiningTrip
{
    public class Character : MonoBehaviour
    {
        public Action CharacterReachedHook;

        //public GameEventCreature CreatureHitCharacter;
        [SerializeField] private float moveSpeed = 3f;
        private bool moving = false;
        private Vector2 target;

        private void OnTriggerEnter2D(Collider2D collision)
        {
//            Creature creature;
//            if (creature = collision.GetComponentInParent<Creature>())
//            {
//                CreatureHitCharacter?.Raise(creature);
//            }
        }

        private void Update()
        {
            if (!moving) return;

            var dir = target.Subtract(transform.position);
            dir = dir.normalized;
            transform.position = transform.position.Add(Time.deltaTime * moveSpeed * dir);
            if (Vector3.Distance(target, transform.position) < 0.2f)
            {
                transform.position = target;
                moving = false;
                CharacterReachedHook?.Invoke();
            }
        }

        public void MoveTo(Vector2 targetToMoveTo)
        {
            moving = true;
            target = targetToMoveTo;
        }
    }
}