﻿using Global;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Managers
{
    public class MenuUIManager : MonoBehaviour
    {
        [SerializeField]
        private Text goldAmountText = default;

        private MenuManager menuManager;


        private void Start()
        {
            menuManager = FindObjectOfType<MenuManager>();
            ShowOres();
        }

        public void OnStartMiningTrip()
        {
            SceneController.Instance.LoadMiningTripScene();
        }

        public void ShowOres()
        {
            goldAmountText.text = GameManager.Instance.GoldAmount.ToString();
        }
    }
}
