﻿using Global;
using UnityEngine;

namespace Menu.Managers
{
    public class MenuManager : MonoBehaviour
    {
        public void StartMiningTrip()
        {
            SceneController.Instance.LoadMiningTripScene();
        }
    }
}
