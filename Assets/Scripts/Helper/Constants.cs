﻿namespace Helper
{
    public static class Constants
    {
        public static class Tags
        {
            public const string CHARACTER = "Character";
            public const string BORDER = "Border";
            public const string HOOK = "Hook";
        }
        public static class SceneIndexes
        {
            public const int GLOBAL_SCENE = 0;
            public const int MENU_SCENE = 1;
            public const int GAME_SCENE = 2;
        }
    }
}