﻿using UnityEngine;

namespace Helper
{
    public static class HelperFunctions
    {
        #region Vector

        public static Quaternion LookAt(Vector2 from, Vector2 to)
        {
            Vector2 fromTo = to - from;
            fromTo = fromTo.normalized;
            float zRotation = Mathf.Atan(fromTo.y / fromTo.x) * Mathf.Rad2Deg;
            zRotation += fromTo.x > 0 ? -90f : 90f;
            return Quaternion.Euler(new Vector3(0, 0, zRotation));
        }

        public static Vector2 Add(this Vector2 a, Vector3 b)
        {
            return new Vector2(a.x + b.x, a.y + b.y);
        }

        public static Vector2 Subtract(this Vector2 a, Vector3 b)
        {
            return new Vector2(a.x - b.x, a.y - b.y);
        }

        public static Vector2 Add(this Vector3 a, Vector2 b)
        {
            return new Vector2(a.x + b.x, a.y + b.y);
        }

        public static Vector2 FindNearestPointOnLine(Vector2 origin, Vector2 direction, Vector2 point) {
            direction.Normalize();
            Vector2 lhs = point - origin;

            float dotP = Vector2.Dot(lhs, direction);
            return origin + direction * dotP;
        }

        #endregion Vector

        public static float GetRotationFromDirection(Vector2 direction)
        {
            var angle = Vector2.Angle(Vector2.up, direction);
            if (direction.x + Mathf.Epsilon < 0.0f)
            {
                angle *= -1f;
                angle += 360;
            }

            return angle;
        }

        public static Vector2 GetDirectionFromRotation(float rotation)
        {
            if (rotation < 0)
            {
                rotation += 360;
            }
            var radians = rotation * Mathf.Deg2Rad;
            var x = Mathf.Sin(radians);
            var y = Mathf.Cos(radians);
            return new Vector2(x, y);
        }

        public static float ClampRotation(float rotation)
        {
            while (rotation < 0)
            {
                rotation += 360;
            }

            while (rotation > 360)
            {
                rotation -= 360;
            }

            return rotation;
        }
    }
}