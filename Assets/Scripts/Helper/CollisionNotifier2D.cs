﻿//using Global.GameEventSystem.Events;

using Global.GameEventSystem.Events;
using UnityEngine;

namespace Helper
{
    [RequireComponent(typeof(Collider2D))]
    public class CollisionNotifier2D : MonoBehaviour
    {
        public GameEventCollider2D OnTriggerStay;
        public GameEventCollider2D OnTriggerExit;
        public GameEventCollider2D OnTriggerEnter;

        public GameEventCollision2D OnCollisionStay;
        public GameEventCollision2D OnCollisionExit;
        public GameEventCollision2D OnCollisionEnter;

        private void OnTriggerStay2D(Collider2D collider)
        {
            OnTriggerStay?.Raise(collider);
        }
        private void OnTriggerExit2D(Collider2D collider)
        {
            OnTriggerExit?.Raise(collider);
        }
        private void OnTriggerEnter2D(Collider2D collider)
        {
            OnTriggerEnter?.Raise(collider);
        }
        private void OnCollisionStay2D(Collision2D collision)
        {
            OnCollisionStay?.Raise(collision);
        }
        private void OnCollisionExit2D(Collision2D collision)
        {
            OnCollisionExit?.Raise(collision);
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnCollisionEnter?.Raise(collision);
        }
    }
}
