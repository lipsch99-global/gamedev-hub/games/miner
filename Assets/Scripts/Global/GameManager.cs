﻿using UnityEngine;

namespace Global
{
    public class GameManager : MonoBehaviour
    {
        public int GoldAmount { get; set; }

        public static GameManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                DestroyImmediate(this);
            }
        }
    }
}
