﻿using Helper;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Global
{
    public class AssureGlobalSceneLoaded : MonoBehaviour
    {
        private void Awake()
        {
            if (SceneController.Instance == null)
            {
                SceneManager.LoadScene(Constants.SceneIndexes.GLOBAL_SCENE, LoadSceneMode.Additive);
            }
        }
    }
}
