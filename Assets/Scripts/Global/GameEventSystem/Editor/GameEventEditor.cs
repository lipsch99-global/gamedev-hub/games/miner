﻿using Global.GameEventSystem.Base;
using UnityEditor;
using UnityEngine;

namespace Global.GameEventSystem.Editor
{
    [CustomEditor(typeof(GameEvent))]
    public class GameEventEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            GameEvent myScript = (GameEvent)target;
            if (GUILayout.Button("Raise"))
            {
                myScript.Raise();
            }
        }
    }
}