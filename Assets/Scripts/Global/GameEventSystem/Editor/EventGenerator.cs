﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MiningTrip.CreatureSystem;
using MiningTrip.HookSystem;
using MiningTrip.MapSystem;
using UnityEditor;
using UnityEngine;

namespace Global.GameEventSystem.Editor
{
    public class GameEventGenerator
    {
        private static string gameEventTemplate =
            @"using Global.GameEventSystem.Base;
using UnityEngine;

namespace Global.GameEventSystem.Events
{

[CreateAssetMenu(menuName = ""Events/#NAME#"")]
public class GameEvent#NAME# : GameEventBase<#TYPE#> { }
}";

        private static string gameEventListenerTemplate =
            @"using UnityEngine;
using UnityEngine.Events;
using System;

public class GameEventListener#NAME# : GameEventListenerBase<#TYPE#>
{
    public GameEvent#NAME# Event;
    public UnityEvent#NAME# Response;

    public override GameEventBase<#TYPE#> GetEvent()
    {
        return Event;
    }

    public override UnityEvent<#TYPE#> GetResponse()
    {
        return Response;
    }
}

[System.Serializable]
public class UnityEvent#NAME# : UnityEvent<#TYPE#> { }";

        private static void CreateScriptFile(string content, string path, string typeName, string name,
            string fileName)
        {
            using (var fs = File.Create(path + "/" + fileName))
            {
                content = content.Replace("#TYPE#", typeName);
                content = content.Replace("#NAME#", name);

                var info = new UTF8Encoding(true).GetBytes(content);
                fs.Write(info, 0, info.Length);
            }
        }

        public static void GenerateScripts(Type[] types, string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string[] subFolders = new string[]
            {
                "Events",
                "Listeners"
            };

            foreach (var folder in subFolders)
            {
                foreach (var file in new DirectoryInfo(Path.Combine(path, folder)).GetFiles())
                {
                    //if (file.Extension.Contains("meta"))
                    //{
                    //    continue;
                    //}

                    file.Delete();
                }
            }

            foreach (var generateType in types)
            {
                var typeName = GetCSharpRepresentation(generateType, true);
                var name = UppercaseFirst(typeName);

                CreateScriptFile(gameEventTemplate, Path.Combine(path, subFolders[0]),
                    typeName, name, "GameEvent" + name + ".cs");
                CreateScriptFile(gameEventListenerTemplate, Path.Combine(path, subFolders[1]),
                    typeName, name, "GameEventListener" + name + ".cs");
            }

            AssetDatabase.Refresh();
        }

        private static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        [MenuItem("Tools/ReGenerate Event Scripts")]
        public static void Generate()
        {
            Type[] types = new Type[]
            {
                typeof(float),
                typeof(int),
                typeof(Ore),
                typeof(Rock),
                typeof(Creature),
                typeof(MapBorder),
                typeof(Collider2D),
                typeof(Collision2D)
            };
            string path = Path.Combine(Application.dataPath, "Scripts/Global/GameEventSystem/");
            GenerateScripts(types, path);
        }

        private static string GetCSharpRepresentation(Type t, bool trimArgCount)
        {
            if (t == typeof(float))
            {
                return "float";
            }
            else if(t == typeof(int))
            {
                return "int";
            }
            if (t.IsGenericType)
            {
                var genericArgs = t.GetGenericArguments().ToList();

                return GetCSharpRepresentation(t, trimArgCount, genericArgs);
            }

            return t.Name;
        }

        private static string GetCSharpRepresentation(Type t, bool trimArgCount, List<Type> availableArguments)
        {
            if (t.IsGenericType)
            {
                string value = t.Name;
                if (trimArgCount && value.IndexOf("`") > -1)
                {
                    value = value.Substring(0, value.IndexOf("`"));
                }

                if (t.DeclaringType != null)
                {
                    // This is a nested type, build the nesting type first
                    value = GetCSharpRepresentation(t.DeclaringType, trimArgCount, availableArguments) + "+" + value;
                }

                // Build the type arguments (if any)
                string argString = "";
                var thisTypeArgs = t.GetGenericArguments();
                for (int i = 0; i < thisTypeArgs.Length && availableArguments.Count > 0; i++)
                {
                    if (i != 0)
                    {
                        argString += ", ";
                    }

                    argString += GetCSharpRepresentation(availableArguments[0], trimArgCount);
                    availableArguments.RemoveAt(0);
                }

                // If there are type arguments, add them with < >
                if (argString.Length > 0)
                {
                    value += "<" + argString + ">";
                }

                return value;
            }

            return t.Name;
        }

    }
}