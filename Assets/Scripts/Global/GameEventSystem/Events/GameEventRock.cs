using Global.GameEventSystem.Base;
using MiningTrip.MapSystem;
using UnityEngine;

namespace Global.GameEventSystem.Events
{

[CreateAssetMenu(menuName = "Events/Rock")]
public class GameEventRock : GameEventBase<Rock> { }
}