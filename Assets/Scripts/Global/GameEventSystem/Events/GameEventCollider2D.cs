using Global.GameEventSystem.Base;
using UnityEngine;

namespace Global.GameEventSystem.Events
{

[CreateAssetMenu(menuName = "Events/Collider2D")]
public class GameEventCollider2D : GameEventBase<Collider2D> { }
}