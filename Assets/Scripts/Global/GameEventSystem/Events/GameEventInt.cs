using Global.GameEventSystem.Base;
using UnityEngine;

namespace Global.GameEventSystem.Events
{

[CreateAssetMenu(menuName = "Events/Int")]
public class GameEventInt : GameEventBase<int> { }
}