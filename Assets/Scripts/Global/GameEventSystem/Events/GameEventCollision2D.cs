using Global.GameEventSystem.Base;
using UnityEngine;

namespace Global.GameEventSystem.Events
{

[CreateAssetMenu(menuName = "Events/Collision2D")]
public class GameEventCollision2D : GameEventBase<Collision2D> { }
}