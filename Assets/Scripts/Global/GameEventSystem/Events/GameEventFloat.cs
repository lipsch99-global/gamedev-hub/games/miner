using Global.GameEventSystem.Base;
using UnityEngine;

namespace Global.GameEventSystem.Events
{

[CreateAssetMenu(menuName = "Events/Float")]
public class GameEventFloat : GameEventBase<float> { }
}