using Global.GameEventSystem.Base;
using MiningTrip.MapSystem;
using UnityEngine;

namespace Global.GameEventSystem.Events
{

[CreateAssetMenu(menuName = "Events/Ore")]
public class GameEventOre : GameEventBase<Ore> { }
}