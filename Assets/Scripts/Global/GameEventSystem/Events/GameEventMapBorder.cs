using Global.GameEventSystem.Base;
using MiningTrip.HookSystem;
using UnityEngine;

namespace Global.GameEventSystem.Events
{

[CreateAssetMenu(menuName = "Events/MapBorder")]
public class GameEventMapBorder : GameEventBase<MapBorder> { }
}