using Global.GameEventSystem.Base;
using MiningTrip.CreatureSystem;
using UnityEngine;

namespace Global.GameEventSystem.Events
{

[CreateAssetMenu(menuName = "Events/Creature")]
public class GameEventCreature : GameEventBase<Creature> { }
}