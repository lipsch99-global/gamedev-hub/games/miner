﻿using UnityEngine;
using UnityEngine.Events;

namespace Global.GameEventSystem.Base
{
    public abstract class GameEventListenerBase<T> : MonoBehaviour
    {
        private void OnEnable()
        {
            GetEvent().RegisterListener(this);
        }

        private void OnDisable()
        {
            GetEvent().UnregisterListener(this);
        }

        public void OnEventRaised(T value)
        {
            GetResponse().Invoke(value);
        }

        public abstract GameEventBase<T> GetEvent();
        public abstract UnityEvent<T> GetResponse();
    }
}
