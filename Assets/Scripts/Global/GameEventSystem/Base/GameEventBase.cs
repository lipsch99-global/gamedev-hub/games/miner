﻿using System.Collections.Generic;
using UnityEngine;

namespace Global.GameEventSystem.Base
{
    public abstract class GameEventBase<T> : ScriptableObject
    {
        private List<GameEventListenerBase<T>> listeners = new List<GameEventListenerBase<T>>();

        public void Raise(T value)
        {
            for (int i = listeners.Count - 1; i >= 0; i--)
            {
                listeners[i].OnEventRaised(value);
            }
        }

        public void RegisterListener(GameEventListenerBase<T> listener)
        {
            listeners.Add(listener);
        }

        public void UnregisterListener(GameEventListenerBase<T> listener)
        {
            listeners.Remove(listener);
        }
    }
}
