﻿using MiningTrip.CreatureSystem;
using UnityEngine;
using UnityEngine.Events;

namespace Global.GameEventSystem.Helper
{
    public class ConditionalRaiseCreature : MonoBehaviour
    {
        public UnityEvent Response;

        private Creature creature;

        private void Start()
        {
            creature = GetComponent<Creature>();
        }

        public void ConditionalRaise(Creature _creature)
        {
            if (creature == _creature)
            {
                Response.Invoke();
            }
        }
    }
}
