﻿using Global.GameEventSystem.Base;
using UnityEngine;

namespace Global.GameEventSystem.Helper
{
    public class RaiseEvent : MonoBehaviour
    {
        [SerializeField]
        private GameEvent _EventToRaise = default;
        public GameEvent EventToRaise
        {
            get
            {
                return _EventToRaise;
            }
        }

        public void Raise()
        {
            _EventToRaise.Raise();
        }
    }
}
