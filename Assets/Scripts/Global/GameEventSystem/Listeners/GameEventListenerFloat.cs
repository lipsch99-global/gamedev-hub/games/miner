using UnityEngine;
using UnityEngine.Events;
using System;
using Global.GameEventSystem.Base;
using Global.GameEventSystem.Events;

public class GameEventListenerFloat : GameEventListenerBase<float>
{
    public GameEventFloat Event;
    public UnityEventFloat Response;

    public override GameEventBase<float> GetEvent()
    {
        return Event;
    }

    public override UnityEvent<float> GetResponse()
    {
        return Response;
    }
}

[System.Serializable]
public class UnityEventFloat : UnityEvent<float> { }