using UnityEngine;
using UnityEngine.Events;
using System;
using Global.GameEventSystem.Base;
using Global.GameEventSystem.Events;

public class GameEventListenerCollider2D : GameEventListenerBase<Collider2D>
{
    public GameEventCollider2D Event;
    public UnityEventCollider2D Response;

    public override GameEventBase<Collider2D> GetEvent()
    {
        return Event;
    }

    public override UnityEvent<Collider2D> GetResponse()
    {
        return Response;
    }
}

[System.Serializable]
public class UnityEventCollider2D : UnityEvent<Collider2D> { }