using UnityEngine;
using UnityEngine.Events;
using System;
using Global.GameEventSystem.Base;
using Global.GameEventSystem.Events;
using MiningTrip.HookSystem;

public class GameEventListenerMapBorder : GameEventListenerBase<MapBorder>
{
    public GameEventMapBorder Event;
    public UnityEventMapBorder Response;

    public override GameEventBase<MapBorder> GetEvent()
    {
        return Event;
    }

    public override UnityEvent<MapBorder> GetResponse()
    {
        return Response;
    }
}

[System.Serializable]
public class UnityEventMapBorder : UnityEvent<MapBorder> { }