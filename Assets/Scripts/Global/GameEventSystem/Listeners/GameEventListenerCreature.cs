using UnityEngine;
using UnityEngine.Events;
using System;
using Global.GameEventSystem.Base;
using Global.GameEventSystem.Events;
using MiningTrip.CreatureSystem;

public class GameEventListenerCreature : GameEventListenerBase<Creature>
{
    public GameEventCreature Event;
    public UnityEventCreature Response;

    public override GameEventBase<Creature> GetEvent()
    {
        return Event;
    }

    public override UnityEvent<Creature> GetResponse()
    {
        return Response;
    }
}

[System.Serializable]
public class UnityEventCreature : UnityEvent<Creature> { }