using UnityEngine;
using UnityEngine.Events;
using System;
using Global.GameEventSystem.Base;
using Global.GameEventSystem.Events;

public class GameEventListenerCollision2D : GameEventListenerBase<Collision2D>
{
    public GameEventCollision2D Event;
    public UnityEventCollision2D Response;

    public override GameEventBase<Collision2D> GetEvent()
    {
        return Event;
    }

    public override UnityEvent<Collision2D> GetResponse()
    {
        return Response;
    }
}

[System.Serializable]
public class UnityEventCollision2D : UnityEvent<Collision2D> { }