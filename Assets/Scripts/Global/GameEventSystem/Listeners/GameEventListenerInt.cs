using UnityEngine;
using UnityEngine.Events;
using System;
using Global.GameEventSystem.Base;
using Global.GameEventSystem.Events;

public class GameEventListenerInt : GameEventListenerBase<int>
{
    public GameEventInt Event;
    public UnityEventInt Response;

    public override GameEventBase<int> GetEvent()
    {
        return Event;
    }

    public override UnityEvent<int> GetResponse()
    {
        return Response;
    }
}

[System.Serializable]
public class UnityEventInt : UnityEvent<int> { }