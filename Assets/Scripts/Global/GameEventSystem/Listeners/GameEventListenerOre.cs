using UnityEngine;
using UnityEngine.Events;
using System;
using Global.GameEventSystem.Base;
using Global.GameEventSystem.Events;
using MiningTrip.MapSystem;

public class GameEventListenerOre : GameEventListenerBase<Ore>
{
    public GameEventOre Event;
    public UnityEventOre Response;

    public override GameEventBase<Ore> GetEvent()
    {
        return Event;
    }

    public override UnityEvent<Ore> GetResponse()
    {
        return Response;
    }
}

[System.Serializable]
public class UnityEventOre : UnityEvent<Ore> { }