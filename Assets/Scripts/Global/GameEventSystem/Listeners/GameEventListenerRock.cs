using UnityEngine;
using UnityEngine.Events;
using System;
using Global.GameEventSystem.Base;
using Global.GameEventSystem.Events;
using MiningTrip.MapSystem;

public class GameEventListenerRock : GameEventListenerBase<Rock>
{
    public GameEventRock Event;
    public UnityEventRock Response;

    public override GameEventBase<Rock> GetEvent()
    {
        return Event;
    }

    public override UnityEvent<Rock> GetResponse()
    {
        return Response;
    }
}

[System.Serializable]
public class UnityEventRock : UnityEvent<Rock> { }