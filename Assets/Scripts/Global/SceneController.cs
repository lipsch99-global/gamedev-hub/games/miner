﻿using System.Collections;
using Global.GameEventSystem.Base;
using Helper;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Global
{
    public class SceneController : MonoBehaviour
    {
        [SerializeField]
        private Image transitionImage = default;
        [SerializeField]
        private float transitionLength = default;

        [SerializeField]
        private GameEvent _SceneChangeStarted = default;
        public GameEvent SceneChangeStarted
        {
            get
            {
                return _SceneChangeStarted;
            }
        }
        [SerializeField]
        private GameEvent _SceneChangeFinished = default;
        public GameEvent SceneChangeFinished
        {
            get
            {
                return _SceneChangeFinished;
            }
        }

        [SerializeField]
        private GameEvent _SceneTransitionFinished = default;
        public GameEvent SceneTransitionFinished
        {
            get
            {
                return _SceneTransitionFinished;
            }
        }

        public static SceneController Instance { get; private set; }

        private AsyncOperation currentSceneLoading;


        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                DestroyImmediate(this);
            }
        }
        private void Start()
        {
            if (SceneManager.sceneCount < 2)
            {
                LoadMenuScene();
            }
            else
            {
                StartCoroutine(EndSceneTransition());
            }
        }


        public void LoadMiningTripScene()
        {
            if (SceneManager.sceneCount > 1)
            {
                SceneManager.UnloadSceneAsync(1);
            }
            currentSceneLoading = SceneManager.LoadSceneAsync(Constants.SceneIndexes.GAME_SCENE, LoadSceneMode.Additive);
            currentSceneLoading.completed += LevelLoadedAsynchronously;
            currentSceneLoading.allowSceneActivation = false;
            StartCoroutine(StartSceneTransition());
        }

        public void LoadMenuScene()
        {
            if (SceneManager.sceneCount > 1)
            {
                SceneManager.UnloadSceneAsync(2);
            }
            currentSceneLoading = SceneManager.LoadSceneAsync(Constants.SceneIndexes.MENU_SCENE, LoadSceneMode.Additive);
            currentSceneLoading.completed += LevelLoadedAsynchronously;
            currentSceneLoading.allowSceneActivation = false;
            StartCoroutine(StartSceneTransition());
        }

        private IEnumerator StartSceneTransition()
        {
            SceneChangeStarted.Raise();
            while (transitionImage.fillAmount < 1)
            {
                transitionImage.fillAmount += Time.deltaTime / transitionLength;
                yield return null;
            }
            currentSceneLoading.allowSceneActivation = true;
        }

        private IEnumerator EndSceneTransition()
        {
            SceneChangeFinished.Raise();
            while (transitionImage.fillAmount > 0)
            {
                transitionImage.fillAmount -= Time.deltaTime / transitionLength;
                yield return null;
            }
            SceneTransitionFinished.Raise();
        }

        private void LevelLoadedAsynchronously(AsyncOperation operation)
        {
            StartCoroutine(EndSceneTransition());
        }
    }
}
