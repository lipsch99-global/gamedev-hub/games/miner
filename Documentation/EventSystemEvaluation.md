# Event System Evaluation

In this document multiple strategies for implementing the event system will be documented and rated to find the best solution.

## Examples of usage
Some examples where this is used to tailor the solution to the needs.

- Hook amount changed
- Hook hit
- Creature triggered
- 

## Approaches
### Scriptable Object
#### Pro
- Easily changeable by inspector
#### Con
- For events with different arguments we need seperate classes. These need to be generated since it would be too tedious to do so manually.
### Classic Delegates
#### Pro
#### Con
